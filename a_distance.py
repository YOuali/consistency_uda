import random, math, os, argparse, pdb, pprint
from os import path as osp
import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import network
import loss
import pre_process as prep
from datasets import ImageList, ConsistencyDataset
import mics

def image_classification_test(loader_source, loader_target, model, ad_net, test_10crop=True):
    with torch.no_grad():
        start_test = True
        for i, loader in enumerate([loader_source, loader_target]):
            iter_test = iter(loader)
            for i in range(len(loader)):
                data = iter_test.next()
                inputs = data[0]
                labels = data[1]
                inputs = inputs.cuda()
                labels = torch.ones(inputs.size(0)) if i == 0 else torch.ones(inputs.size(0))
                features, _ = model(inputs)
                outputs = ad_net(features)[:,0]
                if start_test:
                    all_output = outputs.float().cpu()
                    all_label = labels.float()
                    start_test = False
                else:
                    all_output = torch.cat((all_output, outputs.float().cpu()), 0)
                    all_label = torch.cat((all_label, labels.float()), 0)
        predict = (all_output > 0.5)
        accuracy = torch.sum(torch.squeeze(predict).bool() == all_label.bool()).item() / float(all_label.size()[0])
    return accuracy

def train(config):
    # Set preprocessing
    prep_dict = {}
    prep_config = config["prep"]
    prep_dict["source"] = prep.image_train(**config["prep"])
    prep_dict["target"] = prep.image_train(**config["prep"])
    prep_dict["test"] = prep.image_test(**config["prep"])

    # Create Train Datasets
    dsets = {}
    data_config = config["data"]
    train_bs = data_config["source"]["batch_size"]
    test_bs = data_config["test"]["batch_size"]
    dsets["source"] = ImageList(open(data_config["source"]["list_path"]).readlines(),
                                transform=prep_dict["source"])
    dsets["target"] = ImageList(open(data_config["target"]["list_path"]).readlines(),
                                transform=prep_dict["target"])

    # Create Train Datalaoders
    dset_loaders = {}
    dset_loaders["source"] = DataLoader(dsets["source"], batch_size=train_bs,
                                        shuffle=True, num_workers=4, drop_last=True)
    dset_loaders["target"] = DataLoader(dsets["target"], batch_size=train_bs,
                                        shuffle=True, num_workers=8, drop_last=True)

    # Create Test Datalaoders
    dsets["test_source"] = ImageList(open(data_config["test"]["list_path_source"]).readlines(),
                                transform=prep_dict["test"])
    dset_loaders["test_source"] = DataLoader(dsets["test_source"], batch_size=test_bs,
                                        shuffle=False, num_workers=4)
    dsets["test_target"] = ImageList(open(data_config["test"]["list_path_target"]).readlines(),
                                transform=prep_dict["test"])
    dset_loaders["test_target"] = DataLoader(dsets["test_target"], batch_size=test_bs,
                                        shuffle=False, num_workers=4)

    # Set base network
    class_num = config["network"]["params"]["class_num"]
    net_config = config["network"]
    base_network = net_config["name"](**net_config["params"])
    base_network = base_network.cuda()
    ad_net = network.AdversarialNetwork(base_network.output_num(), 1024)
    ad_net = ad_net.cuda()

    # Reload
    assert config["resume"] is not None
    checkpoint = torch.load(config["resume"])
    base_network.load_state_dict(checkpoint['base_network'])
    base_network.freeze_feature_extractor()

    # Set optimizer
    parameter_list = ad_net.get_parameters()
    optimizer_config = config["optimizer"]
    optimizer = optimizer_config["type"](parameter_list, **(optimizer_config["optim_params"]))
    param_lr = []
    for param_group in optimizer.param_groups:
        param_lr.append(param_group["lr"])
    schedule_param = optimizer_config["lr_param"]
    lr_scheduler = mics.schedule_dict[optimizer_config["lr_type"]]

    # Train
    len_train_source = len(dset_loaders["source"])
    len_train_target = len(dset_loaders["target"])
    avg_transfer_loss = mics.AverageMeter()
    best_acc = 0.0

    total_iters = config["num_iterations"]
    tbar = tqdm(range(total_iters), ncols=135)

    print(f"Training, total iterations {total_iters}")
    print(f"Source epochs: {total_iters // len(dset_loaders['source']) + 1}")
    print(f"Target epochs: {total_iters // len(dset_loaders['target']) + 1}")

    for i in tbar:
        # Sanity check
        assert list(base_network.feature_layers[-2].modules())[-3].weight.requires_grad == False

        # Train one iter
        base_network.train(True)
        ad_net.train(True)
        optimizer = lr_scheduler(optimizer, i, **schedule_param)
        optimizer.zero_grad()

        # Reset dataloaders
        if i % len_train_source == 0:
            iter_source = iter(dset_loaders["source"])
            avg_transfer_loss.reset()
        if i % len_train_target == 0:
            iter_target = iter(dset_loaders["target"])

        # Fetch new batches
        inputs_source, labels_source = iter_source.next()
        inputs_target, labels_target = iter_target.next()
        inputs_source, labels_source = inputs_source.cuda(), labels_source.cuda()
        inputs_target, labels_target = inputs_target.cuda(), labels_target.cuda()
        features_source, outputs_source = base_network(inputs_source)
        features_target, outputs_target = base_network(inputs_target)

        outputs = torch.cat((outputs_source, outputs_target), dim=0)
        softmax_out = nn.Softmax(dim=1)(outputs)

        # Compute losses
        features = torch.cat((features_source, features_target), dim=0)
        transfer_loss = loss.DANN([features, softmax_out], ad_net)
        total_loss = transfer_loss
        avg_transfer_loss.update(transfer_loss)

        total_loss.backward()
        optimizer.step()
        tbar.set_description('Ladv {:.2f} | Best acc |'.format(
            avg_transfer_loss.avg, best_acc))

        # Testing
        if i % config["test_interval"] == config["test_interval"] - 1:
            base_network.train(False)
            ad_net.train(False)
            temp_acc = image_classification_test(dset_loaders["test_source"], dset_loaders["test_target"], base_network, ad_net=ad_net)
            if temp_acc > best_acc: best_acc = temp_acc
            log_str = "iter: {:05d}, precision: {:.5f}".format(i, temp_acc)
            config["out_file"].write(log_str+"\n")
            config["out_file"].flush()
            print(log_str)

    config["out_file"].write("\n Final best acc: source: {:05f} target {:05f}\n\n".format(best_acc, best_acc_t))
    print("\n Final best acc: {:05f} \n\n".format(best_acc))
    return best_acc

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Consistency based domain adaptation')
    # General, model & method
    parser.add_argument('--seed', default=1, type=int)
    parser.add_argument('--method', type=str, default='BASELINE',
                        choices=['BASELINE', 'CDAN', 'DANN', 'TDAN', 'TDAN_tensorial_product', 'TDAN_argmax', 'TDAN_random'])
    parser.add_argument('--net', type=str, default='ResNet50',
                        choices=["ResNet18", "ResNet34", "ResNet50", "ResNet101", "ResNet152"])

    # Training
    parser.add_argument('--num_iterations', default=20000, type=int)
    parser.add_argument('--test_interval', type=int, default=500,
                        help="interval of two continuous test phase")
    parser.add_argument('--resume', type=str, default=None,
                        help="Path to .pth model")

    # Dataset
    parser.add_argument('--dset', type=str, default='office',
                        choices=['office', 'image-clef', 'visda', 'office-home'],
                        help="The dataset or source dataset used")
    parser.add_argument('--s_dset_path', type=str, help="The source dataset path list")
    parser.add_argument('--t_dset_path', type=str, help="The target dataset path list")
    
    # Saving
    parser.add_argument('--snapshot_interval', type=int, default=5000,
                        help="interval of two continuous output model")
    parser.add_argument('--output_dir', type=str,
                        help="output directory of our model (in ../snapshot directory)")
    parser.add_argument('--lr', type=float, default=0.001, help="Learning rate")
    parser.add_argument('--batch_size', default=36, type=int, help='Batch size')

    args = parser.parse_args()

    # Fix cuda, torch & numpy random seeds
    torch.backends.cudnn.benchmark = True
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    torch.cuda.manual_seed(args.seed)

    # Train config
    config = {}
    config["resume"] = args.resume
    config["num_iterations"] = args.num_iterations
    config["test_interval"] = args.test_interval

    # Network Configs
    config["network"] = {"name": network.ResNetFc,
                         "params": {"resnet_name": args.net, "use_bottleneck": True,
                                    "bottleneck_dim": 256, "new_cls": True}}

    # Optimizer
    config["optimizer"] = {"type": optim.SGD,
                            "optim_params": {'lr': args.lr, "momentum": 0.9,
                                                    "weight_decay": 0.0005, "nesterov": True},             
                            "lr_type": "inv",
                            "lr_param": {"lr": args.lr, "gamma": 0.001, "power": 0.75}}

    # Dataset configs
    config["dataset"] = args.dset
    config["data"] = {"source": {"list_path": args.s_dset_path, "batch_size": args.batch_size},
                      "target": {"list_path": args.t_dset_path, "batch_size": args.batch_size},
                      "test": {"list_path_source": args.s_dset_path, "batch_size": 4, "list_path_target": args.t_dset_path}}

    # Preprocessing Configs
    config["prep"] = {"resize_size": 256, "crop_size": 224}


    # Learning Rates per dataset (CDAN optimal parameters)
    if config["dataset"] == "office":
        if ("amazon" in args.s_dset_path and "webcam" in args.t_dset_path) or \
           ("webcam" in args.s_dset_path and "dslr" in args.t_dset_path) or \
           ("webcam" in args.s_dset_path and "amazon" in args.t_dset_path) or \
           ("dslr" in args.s_dset_path and "amazon" in args.t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        elif ("amazon" in args.s_dset_path and "dslr" in args.t_dset_path) or \
             ("dslr" in args.s_dset_path and "webcam" in args.t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.0003 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 31

    elif config["dataset"] == "image-clef":
        config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 12

    elif config["dataset"] == "visda":
        config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 12
        config["data"]["test"]["batch_size"] = 32

    elif config["dataset"] == "office-home":
        config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 65

    else:
        raise ValueError(
            'Dataset cannot be recognized. Please define your own dataset here.')

    # Saving path & Checkpoints
    config["output_for_test"] = True
    config["snapshot_interval"] = args.snapshot_interval
    config["output_path"] = "snapshot/" + args.output_dir
    if not osp.exists(config["output_path"]):
        os.system('mkdir -p '+config["output_path"])
    if not osp.exists(config["output_path"]):
        os.mkdir(config["output_path"])

    # Log file & Saving current config
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(config)
    config["out_file"] = open(osp.join(config["output_path"], "log.txt"), "w")
    config["out_file"].write(str(config)+"\n")
    config["out_file"].flush()

    # Train
    train(config)

#  python a_distance.py --net ResNet50 --dset office --test_interval 500 --test_interval 10 \
#     --s_dset_path data/office/webcam_list.txt --method TDAN \
#     --t_dset_path data/office/amazon_list.txt --output_dir joint_wa_resnet \
#     --seed 0 --resume trained_models/no_consistency/resnet_wa1/best_model.pth