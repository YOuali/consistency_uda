import argparse, sys, os, random, pickle
import os.path as osp
import torch
import torch.nn as nn
from tqdm import tqdm
from torch.utils import data, model_zoo
import numpy as np
import torch.optim as optim
import scipy.misc
import torch.backends.cudnn as cudnn
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from val import evaluate
from model.deeplab import DeeplabMulti, VAT
from model.discriminator import FCDiscriminator
from utils.loss import CrossEntropy2d
from dataset.gta5 import GTA5DataSet
from dataset.cityscapes import cityscapesDataSet
from utils.loss import sigmoid_rampup, softmax_mse_loss

IMG_MEAN = np.array((104.00698793, 116.66876762, 122.67891434), dtype=np.float32)

# URL_model = http://vllab.ucmerced.edu/ytsai/CVPR18/DeepLab_resnet_pretrained_init-f81d91e8.pthz
# BATCH_SIZE = 1
# ITER_SIZE = 1
# NUM_WORKERS = 4
# DATA_DIRECTORY = 
# DATA_LIST_PATH = 
# IGNORE_LABEL = 255
# INPUT_SIZE = '1280,720'
# DATA_DIRECTORY_TARGET = 'ranstad/Datasets/Cityscapes/leftImg8bit_trainvaltest/'
# DATA_LIST_PATH_TARGET = './dataset/cityscapes_list/train.txt'
# INPUT_SIZE_TARGET = '1024,512'
# LEARNING_RATE = 2.5e-4
# MOMENTUM = 0.9
# NUM_CLASSES = 19
# NUM_STEPS = 250000
# NUM_STEPS_STOP = 150000  # early stopping
# POWER = 0.9
# RANDOM_SEED = 1234
# RESTORE_FROM = 
# SAVE_NUM_IMAGES = 2
# SAVE_PRED_EVERY = 5000
# SNAPSHOT_DIR = './snapshots/'
# WEIGHT_DECAY = 0.0005
# LOG_DIR = './log'

# LEARNING_RATE_D = 1e-4
# LAMBDA_SEG = 0.1
# LAMBDA_ADV_TARGET1 = 0.0002
# LAMBDA_ADV_TARGET2 = 0.001
# GAN = 'Vanilla'

# TARGET = 
# SET = 'train'

def get_arguments():
    """Parse all the arguments provided from the CLI.

    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLab-ResNet Network")
    parser.add_argument("--target", type=str, default='cityscapes',
                        help="available options : cityscapes")
    parser.add_argument("--batch-size", type=int, default=1,
                        help="Number of images sent to the network in one step.")
    parser.add_argument("--iter-size", type=int, default=1,
                        help="Accumulate gradients for ITER_SIZE iterations.")
    parser.add_argument("--num-workers", type=int, default=4,
                        help="number of workers for multithread dataloading.")
    parser.add_argument("--data-dir", type=str, default='/mnt/beegfs/home/ouali/ranstad/Datasets/GTA5',
                        help="Path to the directory containing the source dataset.")
    parser.add_argument("--data-list", type=str, default='./dataset/gta5_list/train.txt',
                        help="Path to the file listing the images in the source dataset.")
    parser.add_argument("--ignore-label", type=int, default=255,
                        help="The index of the label to ignore during the training.")
    parser.add_argument("--input-size", type=str, default='1280,720',
                        help="Comma-separated string with height and width of source images.")
    parser.add_argument("--data-dir-target", type=str, default='/mnt/beegfs/home/ouali/ranstad/Datasets/Cityscapes/',
                        help="Path to the directory containing the target dataset.")
    parser.add_argument("--data-list-target", type=str, default='./dataset/cityscapes_list/train.txt',
                        help="Path to the file listing the images in the target dataset.")
    parser.add_argument("--data-list-test", type=str, default='./dataset/cityscapes_list/val.txt',
                        help="Path to the file listing the images in the dataset.")
    parser.add_argument("--input-size-target", type=str, default='1024,512',
                        help="Comma-separated string with height and width of target images.")
    parser.add_argument("--is-training", action="store_true",
                        help="Whether to updates the running means and variances during the training.")
    parser.add_argument("--learning-rate", type=float, default=2.5e-4,
                        help="Base learning rate for training with polynomial decay.")
    parser.add_argument("--learning-rate-D", type=float, default=1e-4,
                        help="Base learning rate for discriminator.")
    parser.add_argument("--lambda-seg", type=float, default=0.1,
                        help="lambda_seg.")
    parser.add_argument("--lambda-adv-target1", type=float, default=0.0002,
                        help="lambda_adv for adversarial training.")
    parser.add_argument("--lambda-adv-target2", type=float, default=0.001,
                        help="lambda_adv for adversarial training.")
    parser.add_argument("--momentum", type=float, default=0.9,
                        help="Momentum component of the optimiser.")
    parser.add_argument("--not-restore-last", action="store_true",
                        help="Whether to not restore last (FC) layers.")
    parser.add_argument("--num-classes", type=int, default=19,
                        help="Number of classes to predict (including background).")
    parser.add_argument("--num-steps", type=int, default=250000,
                        help="Number of training steps.")
    parser.add_argument("--num-steps-stop", type=int, default=150000,
                        help="Number of training steps for early stopping.")
    parser.add_argument("--power", type=float, default=0.9,
                        help="Decay parameter to compute the learning rate.")
    parser.add_argument("--random-mirror", action="store_true",
                        help="Whether to randomly mirror the inputs during the training.")
    parser.add_argument("--random-scale", action="store_true",
                        help="Whether to randomly scale the inputs during the training.")
    parser.add_argument("--random-seed", type=int, default=1234,
                        help="Random seed to have reproducible results.")
    parser.add_argument("--restore-from", type=str, default='model/DeepLab_resnet_pretrained_init-f81d91e8.pth',
                        help="Where restore model parameters from.")
    parser.add_argument("--save-num-images", type=int, default=2,
                        help="How many images to save.")
    parser.add_argument("--save-pred-every", type=int, default=5000,
                        help="Save summaries and checkpoint every often.")
    parser.add_argument("--snapshot-dir", type=str, default='./snapshots/',
                        help="Where to save snapshots of the model.")
    parser.add_argument("--weight-decay", type=float, default=0.0005,
                        help="Regularisation parameter for L2-loss.")
    parser.add_argument("--cpu", action='store_true', help="choose to use cpu device.")
    parser.add_argument("--tensorboard", action='store_true', help="choose whether to use tensorboard.")
    parser.add_argument("--log-dir", type=str, default='./log',
                        help="Path to the directory of log.")
    parser.add_argument("--set", type=str, default='train',
                        help="choose adaptation set.")
    parser.add_argument("--gan", type=str, default='Vanilla',
                        help="choose the GAN objective.")

    parser.add_argument('--aug_severity', default=1, type=int,
                        help='Severity of base augmentation operators')
    parser.add_argument('--num_augs', default=1, type=int,
                        help='Number of augmentations to apply')
    parser.add_argument('--consistency', action='store_true', default=False,
                            help='Use consistency loss')
    parser.add_argument('--ramp_up', default=0.1, type=float,
                            help="Consistency loss rampup periode.")
    parser.add_argument('--mean_teacher', action='store_true', default=False,
                            help='Use a teacher model to generated the target predictions')
    parser.add_argument('--vat', action='store_true', default=False)
    parser.add_argument('--lambda_consistency', default=5.0, type=float)
    parser.add_argument('--tdan', action='store_true', default=False)
    return parser.parse_args()

args = get_arguments()

def lr_poly(base_lr, iter, max_iter, power):
    return base_lr * ((1 - float(iter) / max_iter) ** (power))

def adjust_learning_rate(optimizer, i_iter):
    lr = lr_poly(args.learning_rate, i_iter, args.num_steps, args.power)
    optimizer.param_groups[0]['lr'] = lr
    if len(optimizer.param_groups) > 1:
        optimizer.param_groups[1]['lr'] = lr * 10

def adjust_learning_rate_D(optimizer, i_iter):
    lr = lr_poly(args.learning_rate_D, i_iter, args.num_steps, args.power)
    optimizer.param_groups[0]['lr'] = lr
    if len(optimizer.param_groups) > 1:
        optimizer.param_groups[1]['lr'] = lr * 10

def main():
    """Create the model and start the training."""

    best_miou = 0.0
    ramp_up_end = args.ramp_up * args.num_steps_stop
    device = torch.device("cuda" if not args.cpu else "cpu")
    w, h = map(int, args.input_size.split(','))
    input_size = (w, h)
    w, h = map(int, args.input_size_target.split(','))
    input_size_target = (w, h)
    
    cudnn.enabled = True

    # Create network
    model = DeeplabMulti(num_classes=args.num_classes)
    if args.restore_from[:4] == 'http' :
        saved_state_dict = model_zoo.load_url(args.restore_from)
    else:
        saved_state_dict = torch.load(args.restore_from)
    new_params = model.state_dict().copy()
    for i in saved_state_dict:
        i_parts = i.split('.')
        if not args.num_classes == 19 or not i_parts[1] == 'layer5':
            new_params['.'.join(i_parts[1:])] = saved_state_dict[i]
    model.load_state_dict(new_params)

    model.train()
    model.to(device)
    cudnn.benchmark = True

    if args.vat:
        vat = VAT(model)

    # init D
    model_D1 = FCDiscriminator(num_classes=args.num_classes, tdan=args.tdan).to(device)
    model_D2 = FCDiscriminator(num_classes=args.num_classes, tdan=args.tdan).to(device)

    model_D1.train()
    model_D2.train()

    if not os.path.exists(args.snapshot_dir):
        os.makedirs(args.snapshot_dir)
    out_file = open(osp.join(args.snapshot_dir, "log.txt"), "w")

    # Create Dataloaders
    trainloader = data.DataLoader(
        GTA5DataSet(args.data_dir, args.data_list, max_iters=args.num_steps * args.iter_size * args.batch_size,
                    crop_size=input_size,
                    scale=args.random_scale, mirror=args.random_mirror, mean=IMG_MEAN),
        batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers, pin_memory=True)
    trainloader_iter = enumerate(trainloader)

    targetloader = data.DataLoader(cityscapesDataSet(args.data_dir_target, args.data_list_target,
                                                     max_iters=args.num_steps * args.iter_size * args.batch_size,
                                                     crop_size=input_size_target,
                                                     scale=False, mirror=args.random_mirror, mean=IMG_MEAN,
                                                     set=args.set, aug_severity=args.aug_severity, consistency=args.consistency,
                                                     num_augs=args.num_augs),
                                   batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers,
                                   pin_memory=True)
    targetloader_iter = enumerate(targetloader)

    testloader = data.DataLoader(cityscapesDataSet(args.data_dir_target, args.data_list_test,
                                    crop_size=(1024, 512), mean=IMG_MEAN, scale=False, mirror=False, set="val"),
                                    batch_size=1, shuffle=False, pin_memory=True)

    optimizer = optim.SGD(model.optim_parameters(args),
                          lr=args.learning_rate, momentum=args.momentum, weight_decay=args.weight_decay)
    optimizer.zero_grad()

    optimizer_D1 = optim.Adam(model_D1.parameters(), lr=args.learning_rate_D, betas=(0.9, 0.99))
    optimizer_D1.zero_grad()

    optimizer_D2 = optim.Adam(model_D2.parameters(), lr=args.learning_rate_D, betas=(0.9, 0.99))
    optimizer_D2.zero_grad()

    if args.gan == 'Vanilla':
        reduction='none' if args.tdan else 'mean'
        bce_loss = torch.nn.BCEWithLogitsLoss(reduction=reduction)
    elif args.gan == 'LS':
        bce_loss = torch.nn.MSELoss()
    seg_loss = torch.nn.CrossEntropyLoss(ignore_index=255)

    interp = nn.Upsample(size=(input_size[1], input_size[0]), mode='bilinear', align_corners=True)
    interp_target = nn.Upsample(size=(input_size_target[1], input_size_target[0]), mode='bilinear', align_corners=True)

    # labels for adversarial training
    source_label = 0
    target_label = 1

    # set up tensor board
    if args.tensorboard:
        if not os.path.exists(args.log_dir):
            os.makedirs(args.log_dir)

        writer = SummaryWriter(args.log_dir)

    total_iters = args.num_steps
    tbar = tqdm(range(total_iters), ncols=135)

    for i_iter in tbar:
        loss_seg_value1 = 0
        loss_adv_target_value1 = 0
        loss_D_value1 = 0

        loss_seg_value2 = 0
        loss_adv_target_value2 = 0
        loss_D_value2 = 0
        loss_consistency = 0
        loss_vat = 0

        optimizer.zero_grad()
        adjust_learning_rate(optimizer, i_iter)

        optimizer_D1.zero_grad()
        optimizer_D2.zero_grad()
        adjust_learning_rate_D(optimizer_D1, i_iter)
        adjust_learning_rate_D(optimizer_D2, i_iter)

        for sub_i in range(args.iter_size):
            # train G
            # don't accumulate grads in D
            for param in model_D1.parameters():
                param.requires_grad = False

            for param in model_D2.parameters():
                param.requires_grad = False

            # train with source
            _, batch = trainloader_iter.__next__()
            images, labels, _, _ = batch
            images = images.to(device)
            labels = labels.long().to(device)

            pred1_small, pred2_small = model(images)
            pred1 = interp(pred1_small)
            pred2 = interp(pred2_small)

            loss_seg1 = seg_loss(pred1, labels)
            loss_seg2 = seg_loss(pred2, labels)
            loss = loss_seg2 + args.lambda_seg * loss_seg1

            # proper normalization
            loss = loss / args.iter_size
            loss.backward()
            loss_seg_value1 += loss_seg1.item() / args.iter_size
            loss_seg_value2 += loss_seg2.item() / args.iter_size

            # train with target
            _, batch = targetloader_iter.__next__()
            if args.consistency:
                images, images_aug, labels, _ = batch
                images_aug = images_aug.to(device)
            else:
                images, labels, _ = batch
            images = images.to(device)

            pred_target1_small, pred_target2_small = model(images)
            pred_target1 = interp_target(pred_target1_small)
            pred_target2 = interp_target(pred_target2_small)

            D_out1 = model_D1(F.softmax(pred_target1, dim=1))
            D_out2 = model_D2(F.softmax(pred_target2, dim=1))

            loss_adv_target1 = bce_loss(D_out1, torch.FloatTensor(D_out1.data.size()).fill_(source_label).to(device))
            loss_adv_target2 = bce_loss(D_out2, torch.FloatTensor(D_out2.data.size()).fill_(source_label).to(device))
            if args.tdan:
                pred1_resized = F.interpolate(pred_target1_small.detach().softmax(1), mode="bilinear", size=(16, 32), align_corners=True)
                pred2_resized = F.interpolate(pred_target2_small.detach().softmax(1), mode="bilinear", size=(16, 32), align_corners=True)
                loss_adv_target1 = (pred1_resized * loss_adv_target1).mean(dim=(0,2,3)).sum()
                loss_adv_target2 = (pred2_resized * loss_adv_target2).mean(dim=(0,2,3)).sum()
            
            loss = args.lambda_adv_target1 * loss_adv_target1 + args.lambda_adv_target2 * loss_adv_target2

            if args.consistency:
                pred1_aug, pred2_aug = model(images_aug)
                loss_consistency = softmax_mse_loss(pred1_aug, pred_target1_small.detach())
                loss_consistency += softmax_mse_loss(pred2_aug, pred_target2_small.detach())
                loss_consistency = loss_consistency * sigmoid_rampup(i_iter, ramp_up_end) * args.lambda_consistency
                loss += loss_consistency

            if args.vat:
                pred1_vat, pred2_vat = model(images + vat(images))
                loss_vat = softmax_mse_loss(pred1_vat, pred_target1_small.detach())
                loss_vat += softmax_mse_loss(pred2_vat, pred_target2_small.detach())
                loss_vat = loss_vat * sigmoid_rampup(i_iter, ramp_up_end) * args.lambda_consistency
                loss += loss_vat

            loss = loss / args.iter_size
            loss.backward()
            loss_adv_target_value1 += loss_adv_target1.item() / args.iter_size
            loss_adv_target_value2 += loss_adv_target2.item() / args.iter_size

            # train D
            # bring back requires_grad
            for param in model_D1.parameters():
                param.requires_grad = True

            for param in model_D2.parameters():
                param.requires_grad = True

            # train with source
            pred1 = pred1.detach().softmax(1)
            pred2 = pred2.detach().softmax(1)

            D_out1 = model_D1(pred1)
            D_out2 = model_D2(pred2)

            loss_D1 = bce_loss(D_out1, torch.FloatTensor(D_out1.data.size()).fill_(source_label).to(device))
            loss_D2 = bce_loss(D_out2, torch.FloatTensor(D_out2.data.size()).fill_(source_label).to(device))
            if args.tdan:
                pred1_resized = F.interpolate(pred1, mode="bilinear", size=(22, 40), align_corners=True)
                pred2_resized = F.interpolate(pred2, mode="bilinear", size=(22, 40), align_corners=True)
                loss_D1 = (pred1_resized * loss_D1).mean(dim=(0,2,3)).sum()
                loss_D2 = (pred2_resized * loss_D2).mean(dim=(0,2,3)).sum()

            loss_D1 = loss_D1 / args.iter_size / 2
            loss_D2 = loss_D2 / args.iter_size / 2

            loss_D1.backward()
            loss_D2.backward()

            loss_D_value1 += loss_D1.item()
            loss_D_value2 += loss_D2.item()

            # train with target
            pred_target1 = pred_target1.detach().softmax(1)
            pred_target2 = pred_target2.detach().softmax(1)

            D_out1 = model_D1(pred_target1)
            D_out2 = model_D2(pred_target2)

            loss_D1 = bce_loss(D_out1, torch.FloatTensor(D_out1.data.size()).fill_(target_label).to(device))
            loss_D2 = bce_loss(D_out2, torch.FloatTensor(D_out2.data.size()).fill_(target_label).to(device))
            if args.tdan:
                pred1_resized = F.interpolate(pred_target1, mode="bilinear", size=(16, 32), align_corners=True)
                pred2_resized = F.interpolate(pred_target2, mode="bilinear", size=(16, 32), align_corners=True)
                loss_D1 = (pred1_resized * loss_D1).mean(dim=(0,2,3)).sum()
                loss_D2 = (pred2_resized * loss_D2).mean(dim=(0,2,3)).sum()

            loss_D1 = loss_D1 / args.iter_size / 2
            loss_D2 = loss_D2 / args.iter_size / 2

            loss_D1.backward()
            loss_D2.backward()

            loss_D_value1 += loss_D1.item()
            loss_D_value2 += loss_D2.item()

        optimizer.step()
        optimizer_D1.step()
        optimizer_D2.step()

        if args.tensorboard:
            scalar_info = {
                'loss_seg1': loss_seg_value1,
                'loss_seg2': loss_seg_value2,
                'loss_adv_target1': loss_adv_target_value1,
                'loss_adv_target2': loss_adv_target_value2,
                'loss_D1': loss_D_value1,
                'loss_D2': loss_D_value2,
            }

            if i_iter % 10 == 0:
                for key, val in scalar_info.items():
                    writer.add_scalar(key, val, i_iter)

        tbar.set_description('mIoU {:.3f} | Lseg1 {:.3f} Lseg2 {:.3f} Ladv1 {:.3f}, Ladv2 {:.3f} LD1 {:.3f} LD2 {:.3f} Lconst {:.3f} Lvat {:.3f}|'.format(
            best_miou, loss_seg_value1, loss_seg_value2, loss_adv_target_value1,
            loss_adv_target_value2, loss_D_value1, loss_D_value2, loss_consistency, loss_vat))

        if i_iter >= args.num_steps_stop - 1:
            print('save model ...')
            torch.save(model.state_dict(), osp.join(args.snapshot_dir, 'model.pth'))
            torch.save(model_D1.state_dict(), osp.join(args.snapshot_dir, 'discriminator_D1.pth'))
            torch.save(model_D2.state_dict(), osp.join(args.snapshot_dir, 'discriminator_D2.pth'))
            print("evaluating...")
            miou = evaluate(model, testloader, out_file=out_file)
            if miou > best_miou:
                best_miou = miou
                torch.save(model.state_dict(), osp.join(args.snapshot_dir, 'best_model.pth'))
            break

        if i_iter % args.save_pred_every == 0 and i_iter != 0:
            print('taking snapshot ...')
            torch.save(model.state_dict(), osp.join(args.snapshot_dir, 'model.pth'))
            torch.save(model_D1.state_dict(), osp.join(args.snapshot_dir, 'discriminator_D1.pth'))
            torch.save(model_D2.state_dict(), osp.join(args.snapshot_dir, 'discriminator_D2.pth'))
            print("evaluating...")
            miou = evaluate(model, testloader, out_file=out_file)
            if miou > best_miou:
                best_miou = miou
                torch.save(model.state_dict(), osp.join(args.snapshot_dir, 'best_model.pth'))

    if args.tensorboard:
        writer.close()

if __name__ == '__main__':
    main()
