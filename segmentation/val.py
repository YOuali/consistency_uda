import argparse
import scipy
from scipy import ndimage
import numpy as np
import sys
import torch
from torch.autograd import Variable
import torchvision.models as models
import torch.nn.functional as F
from torch.utils import data, model_zoo
from model.deeplab import DeeplabMulti
from dataset.cityscapes import cityscapesDataSet
from collections import OrderedDict
import os
from PIL import Image
import torch.nn as nn
from utils.metrics import eval_metrics
from tqdm import tqdm

IMG_MEAN = np.array((104.00698793,116.66876762,122.67891434), dtype=np.float32)
RESTORE_FROM = 'http://vllab.ucmerced.edu/ytsai/CVPR18/GTA2Cityscapes_multi-ed35151c.pth'
palette = [128, 64, 128, 244, 35, 232, 70, 70, 70, 102, 102, 156, 190, 153, 153, 153, 153, 153, 250, 170, 30,
           220, 220, 0, 107, 142, 35, 152, 251, 152, 70, 130, 180, 220, 20, 60, 255, 0, 0, 0, 0, 142, 0, 0, 70,
           0, 60, 100, 0, 80, 100, 0, 0, 230, 119, 11, 32]

zero_pad = 256 * 3 - len(palette)
for i in range(zero_pad):
    palette.append(0)

def colorize_mask(mask):
    new_mask = Image.fromarray(mask.astype(np.uint8)).convert('P')
    new_mask.putpalette(palette)
    return new_mask

def get_arguments():
    parser = argparse.ArgumentParser(description="DeepLab-ResNet Network")
    parser.add_argument("--model", type=str, default='DeeplabMulti',
                        help="Model Choice (DeeplabMulti/DeeplabVGG/Oracle).")
    parser.add_argument("--data-dir", type=str, default='./data/Cityscapes/data',
                        help="Path to the directory containing the Cityscapes dataset.")
    parser.add_argument("--data-list", type=str, default='./dataset/cityscapes_list/val.txt',
                        help="Path to the file listing the images in the dataset.")
    parser.add_argument("--ignore-label", type=int, default=255,
                        help="The index of the label to ignore during the training.")
    parser.add_argument("--num-classes", type=int, default=19,
                        help="Number of classes to predict (including background).")
    parser.add_argument("--restore-from", type=str, default=RESTORE_FROM,
                        help="Where restore model parameters from.")
    parser.add_argument("--set", type=str, default='val',
                        help="choose evaluation set.")
    parser.add_argument("--save", type=str, default='./result/cityscapes',
                        help="Path to save result.")
    parser.add_argument("--cpu", action='store_true', help="choose to use cpu device.")
    return parser.parse_args()


def evaluate(model, testloader, save=False, save_path=None, num_classes=19, ignore_index=255, out_file=None):
    model.eval()
    interp = nn.Upsample(size=(1024, 2048), mode='bilinear', align_corners=True)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    total_inter, total_union = 0, 0
    total_correct, total_label = 0, 0

    for index, batch in tqdm(enumerate(testloader), desc="Evaluation, forward pass...", leave=False, dynamic_ncols=True):
        image, target, name = batch
        image, target = image.to(device), target.to(device)

        output1, output2 = model(image)
        output = interp(output2)
        correct, labeled, inter, union = eval_metrics(output, target, num_classes, ignore_index)
        total_inter, total_union = total_inter+inter, total_union+union
        total_correct, total_label = total_correct+correct, total_label+labeled

        if save:
            output = output.cpu().data[0].numpy().transpose(1,2,0)
            output = np.asarray(np.argmax(output, axis=2), dtype=np.uint8)
            output_col = colorize_mask(output)
            output = Image.fromarray(output)
            name = name[0].split('/')[-1]
            output.save('%s/%s' % (save_path, name))
            output_col.save('%s/%s_color.png' % (save_path, name.split('.')[0]))

    pixAcc = 1.0 * total_correct / (np.spacing(1) + total_label)
    IoU = 1.0 * total_inter / (np.spacing(1) + total_union)
    mIoU = IoU.mean()

    if out_file is not None:
        log_str = "pixAcc: {} \n IoU: {} \n mIoU: {:.5f} \n".format(np.round(pixAcc, 3), np.round(IoU, 3), mIoU)
        out_file.write(log_str+"\n")
        out_file.flush()

    print(log_str)
    model.train()
    return mIoU

if __name__ == '__main__':
    args = get_arguments()
    testloader = data.DataLoader(cityscapesDataSet(args.data_dir, args.data_list,
                                    crop_size=(1024, 512), mean=IMG_MEAN, scale=False, mirror=False, set=args.set),
                                    batch_size=1, shuffle=False, pin_memory=True)
    if not os.path.exists(args.save):
        os.makedirs(args.save)

    model = DeeplabMulti(num_classes=args.num_classes)
    if args.restore_from[:4] == 'http' :
        saved_state_dict = model_zoo.load_url(args.restore_from)
    else:
        saved_state_dict = torch.load(args.restore_from)
    
    model_dict = model.state_dict()
    saved_state_dict = {k: v for k, v in saved_state_dict.items() if k in model_dict}
    model_dict.update(saved_state_dict)
    model.load_state_dict(saved_state_dict)
    device = torch.device("cuda" if not args.cpu else "cpu")
    model = model.to(device)

    evaluate(model, testloader, True, args.save, args.num_classes, args.ignore_label)