import os
import os.path as osp
import numpy as np
import random
import matplotlib.pyplot as plt
import collections
import torch
import torchvision
from torch.utils import data
from PIL import Image
from dataset import augmentations
import os, json

def label_mapping(input, mapping):
    output = np.copy(input)
    for ind in range(len(mapping)):
        output[input == mapping[ind][0]] = mapping[ind][1]
    return np.array(output, dtype=np.int64)

class cityscapesDataSet(data.Dataset):
    def __init__(self, root, list_path, max_iters=None, crop_size=(321, 321), mean=(128, 128, 128),
                                        scale=True, mirror=True, ignore_label=255, set='val',
                                        aug_severity=0.8, num_augs=1, consistency=False):
        self.root = root
        self.list_path = list_path
        self.crop_size = crop_size
        self.scale = scale
        self.ignore_label = ignore_label
        self.mean = mean
        self.is_mirror = mirror
        # self.mean_bgr = np.array([104.00698793, 116.66876762, 122.67891434])
        self.img_ids = [i_id.strip() for i_id in open(list_path)]
        if not max_iters==None:
            self.img_ids = self.img_ids * int(np.ceil(float(max_iters) / len(self.img_ids)))
        self.files = []
        self.set = set
        # for split in ["train", "trainval", "val"]:
        for name in self.img_ids:
            img_file = osp.join(self.root, "leftImg8bit_trainvaltest/leftImg8bit/%s/%s" % (self.set, name))
            self.files.append({
                "img": img_file,
                "name": name
            })
            if self.set == "val":
                name_label = name.replace("_leftImg8bit", "_gtFine_labelIds")
                label_file = osp.join(self.root, "gtFine_trainvaltest/gtFine/%s/%s" % (self.set, name_label))
                self.files[-1]["label"] = label_file

        self.aug_severity = aug_severity
        self.num_augs = num_augs
        self.consistency = consistency
    
        if self.set == "val":
            with open("dataset/cityscapes_list/info.json", 'r') as fp:
                info = json.load(fp)
            self.mapping = np.array(info['label2train'], dtype=np.int)

    def __len__(self):
        return len(self.files)

    def __getitem__(self, index):
        datafiles = self.files[index]
        image = Image.open(datafiles["img"]).convert('RGB')
        name = datafiles["name"]

        # resize
        image = image.resize(self.crop_size, Image.BICUBIC)
        if self.consistency:
            image_aug = self.augment(image)
            image_aug = np.asarray(image_aug, np.float32)
            image_aug = image_aug[:, :, ::-1]
            image_aug -= self.mean
            image_aug = image_aug.transpose((2, 0, 1))

        image = np.asarray(image, np.float32)
        size = image.shape
        image = image[:, :, ::-1] #BGR
        image -= self.mean
        image = image.transpose((2, 0, 1))

        if self.consistency:
            return image.copy(), image_aug.copy(), np.array(size), name

        if self.set == "val":
            label = np.array(Image.open(datafiles["label"]))
            label = label_mapping(label, self.mapping)
            return image.copy(), label.copy(), name

        return image.copy(), np.array(size), name

    def augment(self, image):
        aug_list = augmentations.augmentations
        convex_weights = np.float32(np.random.dirichlet([1] * self.num_augs))

        aug_choices = list(range(1, self.num_augs+1)) + [1] * (self.num_augs - 1)
        final_image = np.float32(np.zeros_like(image))

        for i in range(self.num_augs):
            image_aug = image.copy()
            depth = np.random.choice(aug_choices)

            for _ in range(depth):
                operation = np.random.choice(aug_list)
                image_aug = operation(image_aug, self.aug_severity)
            final_image += convex_weights[i] * image_aug
        return final_image

if __name__ == '__main__':
    dst = GTA5DataSet("./data", is_transform=True)
    trainloader = data.DataLoader(dst, batch_size=4)
    for i, data in enumerate(trainloader):
        imgs, labels = data
        if i == 0:
            img = torchvision.utils.make_grid(imgs).numpy()
            img = np.transpose(img, (1, 2, 0))
            img = img[:, :, ::-1]
            plt.imshow(img)
            plt.show()
