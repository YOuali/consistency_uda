import argparse, os, random
from tqdm import tqdm
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
import loss as loss_func
import network
from datasets import ImageList, ConsistencyDataset
import mics
import augmentations


def train(args, model, ad_net, train_loader_source, train_loader_target, optimizer,
                optimizer_ad, epoch, start_epoch, transfer_loss_func, ema_model=None):
    model.train()
    if ema_model is not None:
        ema_model.train()

    # Set averages
    avg_classifier_loss = mics.AverageMeter()
    avg_transfer_loss = mics.AverageMeter()
    avg_consistency_loss = mics.AverageMeter()
    avg_total_loss = mics.AverageMeter()

    len_source = len(train_loader_source)
    len_target = len(train_loader_target)
    num_iter = len_source if len_source > len_target else len_target
    total_iters = num_iter * args.epochs

    tbar = tqdm(range(num_iter), ncols=135)
    for batch_idx in tbar:
        cur_iter = num_iter*(epoch-start_epoch)+batch_idx
        if batch_idx % len_source == 0:
            iter_source = iter(train_loader_source)    
        if batch_idx % len_target == 0:
            iter_target = iter(train_loader_target)

        # Fetch data
        data_source, label_source = iter_source.next()
        data_source, label_source = data_source.cuda(), label_source.cuda()
        data_target, _ = iter_target.next()
        if args.consistency:
            data_target, data_target_aug = data_target[0], data_target[1]
        data_target = data_target.cuda()

        optimizer.zero_grad()
        optimizer_ad.zero_grad()

        # Forward pass
        feature, output = model(torch.cat((data_source, data_target), 0))
        if ema_model is not None:
            _, output_target = ema_model(data_target.cuda())
            assert output[data_source.size(0):].shape == output_target.shape 
            output[data_source.size(0):] = output_target
        softmax_output = nn.Softmax(dim=1)(output)

        # Use source labels
        if args.use_source_labels and args.method == "TDAN":
            softmax_out_source = torch.zeros_like(softmax_output[:data_source.size(0)])
            softmax_out_source[torch.arange(data_source.size(0)), label_source] = 1.0
            softmax_output[:data_source.size(0)] = softmax_out_source

        # Compute losses
        classifier_loss = nn.CrossEntropyLoss()(output.narrow(0, 0, data_source.size(0)), label_source)
        total_loss = classifier_loss
        avg_classifier_loss.update(classifier_loss)

        # Consistency loss
        if args.consistency:
            output_target = output[data_source.size(0):]
            feature_source, output_target_aug = model(data_target_aug.cuda())
            consistency_loss = loss_func.softmax_mse_loss(output_target_aug, output_target.detach())
            consistency_loss = consistency_loss * mics.sigmoid_rampup(cur_iter, int(args.ramp_up * total_iters)) * args.lambda_consistency
            total_loss +=consistency_loss
            avg_consistency_loss.update(consistency_loss)
            del data_target, data_target_aug, data_source

        # Transfer loss
        if transfer_loss_func is not None and epoch > start_epoch:
            coeff = network.calc_coeff(cur_iter) if args.entropy else None
            transfer_loss = transfer_loss_func([feature, softmax_output], ad_net, args.entropy, coeff)
            if args.consistency_on_domain:
                feature = torch.cat((feature_source, feature[data_source.size(0):]), 0)
                transfer_loss = (transfer_loss_func([feature, softmax_output], ad_net, args.entropy, coeff) + transfer_loss) / 2.
            transfer_loss = transfer_loss * args.lambda_transfer
            total_loss += transfer_loss
            avg_transfer_loss.update(transfer_loss)

        # Confidence loss
        if args.confidence:
            confidence_loss = loss_func.confidence_loss(output_target)
            total_loss += confidence_loss * args.lambda_confidence

        # Optimizer step
        total_loss.backward()
        optimizer.step()
        if epoch > start_epoch:
            optimizer_ad.step()
        if args.mean_teacher:
            mics.update_ema_variables(model, ema_model, cur_iter)

        tbar.set_description('Epoch {} | L {:.2f} | Lcls {:.2f} Ltsf {:.2f} Lcons {:.2f} |'.format(
            epoch, avg_total_loss.avg, avg_classifier_loss.avg,
            avg_transfer_loss.avg, avg_consistency_loss.avg))

def test(args, model, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
            data, target = data.cuda(), target.cuda()
            feature, output = model(data)
            test_loss += nn.CrossEntropyLoss()(output, target).item()
            pred = output.data.cpu().max(1, keepdim=True)[1]
            correct += pred.eq(target.data.cpu().view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)
    accuracy = 100. * correct / len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.3f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset), accuracy))
    return accuracy

def main():
    # Training settings
    parser = argparse.ArgumentParser(description='Consistency based domain adaptation - usps & mnist')
    parser.add_argument('--seed', default=1, type=int)
    parser.add_argument('--method', type=str, default='BASELINE',
                        choices=['BASELINE', 'CDAN', 'DANN', 'TDAN'])
    parser.add_argument('--task', default='USPS2MNIST', help='task to perform')
    parser.add_argument('--batch_size', type=int, default=64,
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test_batch_size', type=int, default=1000,
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--entropy', default=False, action="store_true",
                                    help="Add entropy weighting (as in CDAN)")
    parser.add_argument('--confidence', default=False, action="store_true",
                                    help="Add confidence loss to avoid confident predictions on target")
    parser.add_argument('--log_interval', type=int, default=10,
                        help='how many batches to wait before logging training status')

    # Loss trade-offs
    parser.add_argument('--lambda_transfer', default=1.0, type=float,
                                help='Loss trade-off for transfer term')
    parser.add_argument('--lambda_consistency', default=5.0, type=float,
                                help='Loss trade-off for consistency term')
    parser.add_argument('--lambda_confidence', default=0.4, type=float,
                                help='Loss trade-off for confidence term')

    # Consistency args
    parser.add_argument('--aug_severity', default=1, type=int,
                        help='Severity of base augmentation operators')
    parser.add_argument('--num_augs', default=1, type=int,
                        help='Number of augmentations to apply')
    parser.add_argument('--consistency', action='store_true', default=False,
                            help='Use consistency loss')
    parser.add_argument('--possible_augs', default=-1, type=int,
                            help='Size of the set of augmentations to sample from (-1: all of 13 augs)')
    parser.add_argument('--ramp_up', default=0.2, type=float,
                            help="Consistency loss rampup periode (ramps from 0. to 1. in the first 35 percent of iters)")
    parser.add_argument('--mean_teacher', action='store_true', default=False,
                            help='Use a teacher model to generated the target predictions')
    parser.add_argument('--use_source_labels', action='store_true', default=True,
                            help='Use source labels for TDAN.')
    parser.add_argument('--consistency_on_domain', action='store_true', default=False,
                            help='Use consistency on domain discriminator.')

    args = parser.parse_args()

    # Fix cuda, torch & numpy random seeds
    torch.backends.cudnn.benchmark = True
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)

    # Set data paths & decay_epoch
    if args.task == 'USPS2MNIST':
        source_list = 'data/usps2mnist/usps_train.txt'
        target_list = 'data/usps2mnist/mnist_train.txt'
        test_list = 'data/usps2mnist/mnist_test.txt'
        start_epoch = 1
        decay_epoch = 6
    elif args.task == 'MNIST2USPS':
        source_list = 'data/usps2mnist/mnist_train.txt'
        target_list = 'data/usps2mnist/usps_train.txt'
        test_list = 'data/usps2mnist/usps_test.txt'
        start_epoch = 1
        decay_epoch = 5
    else:
        raise Exception('task cannot be recognized!')

    # Set dataloaders
    train_loader_source = torch.utils.data.DataLoader(
        ImageList(open(source_list).readlines(), transform=transforms.Compose([
                           transforms.Resize((28,28)),
                           transforms.ToTensor(),
                           transforms.Normalize((0.5,), (0.5,)) ]), mode='L'),
                        batch_size=args.batch_size, shuffle=True, num_workers=1, drop_last=True)

    # In case of consistency regularization
    if args.consistency:
        augmentations.IMAGE_SIZE = 28
        params_consistency = {"aug_severity": args.aug_severity, "num_augs": args.num_augs,
                                "possible_augs": args.possible_augs}
        base_dataset = ImageList(open(target_list).readlines(), transform=transforms.Resize((28,28)), mode='L')
        consistency_dataset = ConsistencyDataset(base_dataset, preprocess=transforms.Compose([
                            transforms.ToTensor(),
                            transforms.Normalize((0.5,), (0.5,))
                        ]), config=params_consistency)
        train_loader_target = torch.utils.data.DataLoader(consistency_dataset,
                            batch_size=args.batch_size, shuffle=True, num_workers=2, drop_last=True)
    
    else:
        train_loader_target = torch.utils.data.DataLoader(
            ImageList(open(target_list).readlines(), transform=transforms.Compose([
                            transforms.Resize((28,28)),
                            transforms.ToTensor(),
                            transforms.Normalize((0.5,), (0.5,)) ]), mode='L'),
                        batch_size=args.batch_size, shuffle=True, num_workers=1, drop_last=True)

    test_loader = torch.utils.data.DataLoader(
        ImageList(open(test_list).readlines(), transform=transforms.Compose([
                           transforms.Resize((28,28)),
                           transforms.ToTensor(),
                           transforms.Normalize((0.5,), (0.5,)) ]), mode='L'),
                            batch_size=args.test_batch_size, shuffle=True, num_workers=1)

    # Model
    model = network.LeNet()
    model = model.cuda()

    # Mean teacher
    if args.mean_teacher:
        assert args.consistency, "mean teacher works only with consistency"
        ema_model = network.LeNet().cuda()
        ema_model.ema_detach()
    else:
        ema_model = None

    # Domain discriminator
    class_num = 10
    if args.method == "TDAN":
        ad_net = network.AdversarialNetwork(model.output_num(), 1024, class_num)
    elif args.method == "CDAN":
        ad_net = network.AdversarialNetwork(model.output_num() * class_num, 1024)
    else:
        ad_net = network.AdversarialNetwork(model.output_num(), 1024)
    ad_net = ad_net.cuda()

    # Set loss 
    if args.method != "BASELINE":
        transfer_loss_func = loss_func.__dict__[args.method]
    else:
        transfer_loss_func = None

    # Set optimizers
    optimizer = optim.SGD(model.parameters(), lr=args.lr, weight_decay=0.0005, momentum=0.9)
    optimizer_ad = optim.SGD(ad_net.parameters(), lr=args.lr, weight_decay=0.0005, momentum=0.9)

    accuracies = []
    for epoch in range(1, args.epochs + 1):
        if epoch % decay_epoch == 0:
            for param_group in optimizer.param_groups:
                param_group["lr"] = param_group["lr"] * 0.5
        train(args, model, ad_net, train_loader_source, train_loader_target, optimizer,
                        optimizer_ad, epoch, 0, transfer_loss_func, ema_model)
        accuracies.append(test(args, model, test_loader))
    print("\n Final best acc: {}".format(max(accuracies)))

if __name__ == '__main__':
    main()
