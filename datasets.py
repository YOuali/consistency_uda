import torch
import numpy as np
import random
from PIL import Image
from torch.utils.data import Dataset
import os
import os.path
import augmentations


def make_dataset(image_list, labels):
    if labels:
        len_ = len(image_list)
        images = [(image_list[i].strip(), labels[i, :]) for i in range(len_)]
    else:
        if len(image_list[0].split()) > 2:
            images = [(val.split()[0], np.array([int(la) for la in val.split()[1:]])) for val in image_list]
        else:
            images = [(val.split()[0], int(val.split()[1]))
                      for val in image_list]
    return images


def rgb_loader(path):
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')


def l_loader(path):
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('L')


class ImageList(Dataset):
    def __init__(self, image_list, labels=None, transform=None, target_transform=None, mode='RGB'):
        imgs = make_dataset(image_list, labels)
        if len(imgs) == 0:
            raise(RuntimeError("Found 0 images in subfolders of: " + root + "\n"
                               "Supported image extensions are: " + ",".join(IMG_EXTENSIONS)))

        self.imgs = imgs
        self.transform = transform
        self.target_transform = target_transform
        if mode == 'RGB':
            self.loader = rgb_loader
        elif mode == 'L':
            self.loader = l_loader

    def __getitem__(self, index):
        path, target = self.imgs[index]
        img = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.imgs)


class ConsistencyDataset(Dataset):
    """Dataset wrapper to perform consistency augmentations."""

    def __init__(self, dataset, preprocess, config):
        """
        preprocess: standard preprocessing, takes PIL image and returns a torch.tensor
        """
        self.dataset = dataset
        self.preprocess = preprocess
        self.aug_severity = config["aug_severity"]
        self.num_augs = config["num_augs"]
        possible_augs = config["possible_augs"]

        if possible_augs < len(augmentations.augmentations) and possible_augs > 0:
            self.possible_augmentations = np.random.permutation(augmentations.augmentations)[:possible_augs]
        else:
            self.possible_augmentations = augmentations.augmentations

    def augment(self, image):
        aug_list = self.possible_augmentations
        convex_weights = np.float32(np.random.dirichlet([1] * self.num_augs))

        aug_choices = list(range(1, self.num_augs+1)) + [1] * (self.num_augs - 1)
        final_image = torch.zeros_like(self.preprocess(image))

        for i in range(self.num_augs):
            image_aug = image.copy()
            depth = np.random.choice(aug_choices)

            for _ in range(depth):
                operation = np.random.choice(aug_list)
                image_aug = operation(image_aug, self.aug_severity)

            final_image += convex_weights[i] * self.preprocess(image_aug)
        return final_image

    def __getitem__(self, i):
        x, y = self.dataset[i]
        im_tuple = (self.preprocess(x), self.augment(x))
        return im_tuple, y

    def __len__(self):
        return len(self.dataset)
