import random, math, os, argparse, pdb, pprint
from os import path as osp
import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import network
import loss
import pre_process as prep
from datasets import ImageList, ConsistencyDataset
import mics

def image_classification_test(loader, model, test_10crop=True):
    start_test = True
    with torch.no_grad():
        if test_10crop:
            iter_test = [iter(loader['test'][i]) for i in range(10)]
            for i in range(len(loader['test'][0])):
                data = [iter_test[j].next() for j in range(10)]
                inputs = [data[j][0] for j in range(10)]
                labels = data[0][1]
                for j in range(10):
                    inputs[j] = inputs[j].cuda()
                labels = labels
                outputs = []
                for j in range(10):
                    _, predict_out = model(inputs[j])
                    outputs.append(nn.Softmax(dim=1)(predict_out))
                outputs = sum(outputs)
                if start_test:
                    all_output = outputs.float().cpu()
                    all_label = labels.float()
                    start_test = False
                else:
                    all_output = torch.cat((all_output, outputs.float().cpu()), 0)
                    all_label = torch.cat((all_label, labels.float()), 0)
        else:
            iter_test = iter(loader["test"])
            for i in range(len(loader['test'])):
                data = iter_test.next()
                inputs = data[0]
                labels = data[1]
                inputs = inputs.cuda()
                labels = labels.cuda()
                _, outputs = model(inputs)
                if start_test:
                    all_output = outputs.float().cpu()
                    all_label = labels.float()
                    start_test = False
                else:
                    all_output = torch.cat(
                        (all_output, outputs.float().cpu()), 0)
                    all_label = torch.cat((all_label, labels.float()), 0)
    _, predict = torch.max(all_output, 1)
    accuracy = torch.sum(torch.squeeze(predict).float() == all_label).item() / float(all_label.size()[0])
    return accuracy


def train(config):
    # Set preprocessing
    prep_dict = {}
    prep_config = config["prep"]
    prep_dict["source"] = prep.image_train(**config["prep"]['params'])
    # In case of consitency, divide the trasnformations into two sections
    if config['loss']['consistency']:
        preprocess = prep.preprocess(**config["prep"]['params'])
        prep_dict["target"] = prep.image_train_consistency(**config["prep"]['params'])
    else:
        prep_dict["target"] = prep.image_train(**config["prep"]['params'])
    if prep_config["test_10crop"]:
        prep_dict["test"] = prep.image_test_10crop(**config["prep"]['params'])
    else:
        prep_dict["test"] = prep.image_test(**config["prep"]['params'])

    # Create Train Datasets
    dsets = {}
    data_config = config["data"]
    train_bs = data_config["source"]["batch_size"]
    test_bs = data_config["test"]["batch_size"]
    dsets["source"] = ImageList(open(data_config["source"]["list_path"]).readlines(),
                                transform=prep_dict["source"])
    # In case of consistency, add a wrapper for augmentations
    if config['loss']['consistency']:
        target_base_ds = ImageList(open(data_config["target"]["list_path"]).readlines(),
                                    transform=prep_dict["target"])
        dsets["target"] = ConsistencyDataset(dataset=target_base_ds, preprocess=preprocess,
                                    config=config["consistency"])
    else:
        dsets["target"] = ImageList(open(data_config["target"]["list_path"]).readlines(),
                                    transform=prep_dict["target"])

    # Create Train Datalaoders
    dset_loaders = {}
    dset_loaders["source"] = DataLoader(dsets["source"], batch_size=train_bs,
                                        shuffle=True, num_workers=4, drop_last=True)
    dset_loaders["target"] = DataLoader(dsets["target"], batch_size=train_bs,
                                        shuffle=True, num_workers=8, drop_last=True)

    # Create Test Datalaoders
    if prep_config["test_10crop"]:
        for i in range(10):
            dsets["test"] = [ImageList(open(data_config["test"]["list_path"]).readlines(),
                                       transform=prep_dict["test"][i]) for i in range(10)]
            dset_loaders["test"] = [DataLoader(dset, batch_size=test_bs,
                                               shuffle=False, num_workers=8) for dset in dsets['test']]
    else:
        dsets["test"] = ImageList(open(data_config["test"]["list_path"]).readlines(),
                                  transform=prep_dict["test"])
        dset_loaders["test"] = DataLoader(dsets["test"], batch_size=test_bs,
                                          shuffle=False, num_workers=4)

    # Set base network
    class_num = config["network"]["params"]["class_num"]
    net_config = config["network"]
    base_network = net_config["name"](**net_config["params"])
    base_network = base_network.cuda()
    if config['vat'] or config["sensitivity"]:
        vat = network.VAT(base_network)

    # Fase SWA
    if config["fast_swa"]:
        fastswa_basenet = net_config["name"](**net_config["params"]).cuda()
        fastswa_basenet.ema_detach()
        fastswa_optim = mics.WeightSWA(fastswa_basenet)
        fastswa_basenet.train(False)

    # Mean teacher
    if config['mean_teacher']:
        ema_base_network = net_config["name"](**net_config["params"]).cuda()
        ema_base_network.ema_detach()

    # Domain discriminator
    if config["method"] == "TDAN":
        ad_net = network.AdversarialNetwork(base_network.output_num(), 1024, class_num)
    elif config["method"] == "CDAN":
        ad_net = network.AdversarialNetwork(base_network.output_num() * class_num, 1024)
    elif config["method"] == "TDAN_tensorial_product":
        ad_net = network.AdversarialNetwork(base_network.output_num() * class_num, 1024, class_num)
    else:
        ad_net = network.AdversarialNetwork(base_network.output_num(), 1024)
    ad_net = ad_net.cuda()

    # Reload
    if config["resume"] is not None:
        print("Loaded a the pretrained model")
        checkpoint = torch.load(config["resume"])
        base_network.load_state_dict(checkpoint['base_network'])
        ad_net.load_state_dict(checkpoint['ad_net'])
        if config["train_classifier"]:
            # For beta calculation
            base_network.freeze_feature_extractor()
            base_network.init_classifier()

    # Data parallel
    if torch.cuda.device_count() > 1:
        ad_net = nn.DataParallel(ad_net)
        base_network = nn.DataParallel(base_network)
        if config["mean_teacher"]:
            ema_base_network = nn.DataParallel(ema_base_network)
        parameter_list = base_network.module.get_parameters() + ad_net.module.get_parameters()
    else:
        parameter_list = base_network.get_parameters() + ad_net.get_parameters()

    # Set optimizer
    optimizer_config = config["optimizer"]
    optimizer = optimizer_config["type"](parameter_list, **(optimizer_config["optim_params"]))
    param_lr = []
    for param_group in optimizer.param_groups:
        param_lr.append(param_group["lr"])
    schedule_param = optimizer_config["lr_param"]
    lr_scheduler = mics.schedule_dict[optimizer_config["lr_type"]]

    # Set loss 
    loss_params = config["loss"]
    if config['method'] != "BASELINE" and not config["train_classifier"]:
        transfer_loss_func = loss.__dict__[config['method']]
    else:
        transfer_loss_func = None
    # ramp_end = int(loss_params["ramp_up"] * config["num_iterations"])
    ramp_end = loss_params["ramp_up"]

    # Train
    len_train_source = len(dset_loaders["source"])
    len_train_target = len(dset_loaders["target"])
    avg_classifier_loss = mics.AverageMeter()
    avg_transfer_loss = mics.AverageMeter()
    avg_consistency_loss = mics.AverageMeter()
    avg_total_loss = mics.AverageMeter()
    best_acc = 0.0
    temp_acc = 0.0

    total_iters = config["num_iterations"]
    tbar = tqdm(range(total_iters), ncols=135)

    print(f"Training, total iterations {total_iters}")
    print(f"Source epochs: {total_iters // len(dset_loaders['source']) + 1}")
    print(f"Target epochs: {total_iters // len(dset_loaders['target']) + 1}")

    for i in tbar:
        if config["train_classifier"]:
            # Sanity check
            assert list(base_network.feature_layers[-2].modules())[-3].weight.requires_grad == False

        # Train one iter
        base_network.train(True)
        ad_net.train(True)
        if config["mean_teacher"]:
            ema_base_network.train(True)
        optimizer = lr_scheduler(optimizer, i, **schedule_param)
        optimizer.zero_grad()

        # Reset dataloaders
        if i % len_train_source == 0:
            iter_source = iter(dset_loaders["source"])
            avg_classifier_loss.reset(); avg_transfer_loss.reset()
            avg_consistency_loss.reset(); avg_total_loss.reset()
        if i % len_train_target == 0:
            iter_target = iter(dset_loaders["target"])

        # Fetch new batches
        inputs_source, labels_source = iter_source.next()
        inputs_target, labels_target = iter_target.next()

        # Forward pass
        if loss_params['consistency']:
            inputs_target, inputs_target_aug = inputs_target[0], inputs_target[1]
        inputs_source, inputs_target, labels_source = inputs_source.cuda(), inputs_target.cuda(), labels_source.cuda()
        features_source, outputs_source = base_network(inputs_source)
        features_target, outputs_target = base_network(inputs_target)
        if config["mean_teacher"]:
            _, outputs_target = ema_base_network(inputs_target)
        del inputs_source

        # Outputs
        features = torch.cat((features_source, features_target), dim=0)
        outputs = torch.cat((outputs_source, outputs_target), dim=0)
        if config["label_smooth"]:
            outputs_target = outputs_target.softmax(1).detach()
            ls_pos, ls_neg =config["smooth_value"], config["smooth_value"] / (outputs_target.size(1) - 1)
            outputs_target = outputs_target + ls_neg
            outputs_target[torch.arange(outputs_target.size(0)), outputs_target.argmax(1)] -= (ls_pos + ls_neg)
            softmax_out = torch.cat((outputs_source.softmax(1), outputs_target), dim=0)
        else:
            softmax_out = nn.Softmax(dim=1)(outputs)

        # Use source labels
        if loss_params['use_source_labels'] and config['method'] == "TDAN":
            softmax_out_source = torch.zeros_like(outputs_source)
            softmax_out_source[torch.arange(outputs_source.size(0)), labels_source] = 1.0
            softmax_out[:softmax_out_source.size(0)] = softmax_out_source

        # One hot target
        if config["one_hot_target"]:
            with torch.no_grad():
                softmax_target = torch.zeros_like(outputs_target)
                softmax_target[torch.arange(outputs_target.size(0)), outputs_target.argmax(1)] = 1.0
                softmax_out[outputs_target.size(0):] = softmax_target

        # Compute losses
        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)
        if config["train_classifier_on_target"]:
            classifier_loss = (classifier_loss + nn.CrossEntropyLoss()(outputs_target, labels_target)) / 2.
        total_loss = classifier_loss
        avg_classifier_loss.update(classifier_loss)

        # Consistency loss
        if loss_params['consistency'] or config["vat"]:
            if config["mean_teacher"]:
                _, outputs_target = ema_base_network(inputs_target)

            if loss_params['consistency']:
                features_target_aug, outputs_target_aug = base_network(inputs_target_aug.cuda())
                consistency_loss = loss.softmax_mse_loss(outputs_target_aug, outputs_target.detach())
                consistency_loss = consistency_loss * mics.sigmoid_rampup(i, ramp_end) * loss_params["lambda_consistency"]
                del outputs_target_aug

            if config["vat"]:
                r_adv = vat(inputs_target)
                inputs_target_vat = inputs_target + r_adv
                features_target_vat, outputs_target_vat = base_network(inputs_target_vat)
                consistency_loss_vat = loss.softmax_mse_loss(outputs_target_vat, outputs_target.detach())
                consistency_loss_vat = consistency_loss_vat * mics.sigmoid_rampup(i, ramp_end) * loss_params["lambda_consistency"] * 0.25
                consistency_loss = consistency_loss_vat + consistency_loss if loss_params['consistency'] else consistency_loss_vat
                del r_adv, inputs_target_vat, outputs_target_vat,

            total_loss +=consistency_loss
            avg_consistency_loss.update(consistency_loss)
            del inputs_target

        elif config["sensitivity"]:
            r_adv = vat(inputs_target)
            inputs_target_aug = inputs_target + r_adv
            with torch.no_grad():
                _, outputs_target_aug = base_network(inputs_target_aug)
                softmax_target_aug = torch.zeros_like(outputs_target_aug)
                softmax_target_aug[torch.arange(outputs_target_aug.size(0)), outputs_target_aug.argmax(1)] = 1.0
                softmax_out[outputs_target_aug.size(0):] = softmax_target_aug
            del inputs_target_aug, inputs_target, r_adv

        # Transfer loss
        if transfer_loss_func is not None:
            coeff = network.calc_coeff(i) if loss_params['entropy'] else None
            transfer_loss = transfer_loss_func([features, softmax_out], ad_net, loss_params['entropy'], coeff)
            # Train domain discriminator on augmented features
            if loss_params['consistency_on_domain']:
                features = torch.cat((features_source, features_target_aug), dim=0)
                transfer_loss = (transfer_loss_func([features, softmax_out], ad_net, loss_params['entropy'], coeff) + transfer_loss) / 2.
            transfer_loss = transfer_loss * loss_params["lambda_transfer"]
            total_loss += transfer_loss
            avg_transfer_loss.update(transfer_loss)
            del features, softmax_out

        # Confidence loss
        if loss_params['confidence']:
            confidence_loss = loss.confidence_loss(outputs_target)
            total_loss += confidence_loss * loss_params["lambda_confidence"]

        avg_total_loss.update(total_loss)
        total_loss.backward()
        optimizer.step()
        if config["mean_teacher"]:
            mics.update_ema_variables(base_network, ema_base_network, i)

        tbar.set_description('L {:.2f} | Lcls {:.2f} Ltsf {:.2f} Lcons {:.2f} | Last acc {:.2f} Best acc {:.2f} |'.format(
            avg_total_loss.avg, avg_classifier_loss.avg, avg_transfer_loss.avg,
            avg_consistency_loss.avg, temp_acc, best_acc))

        # Stochasitc Weight Averaging
        if config["fast_swa"] and i >= schedule_param["iter_start_swa"]:
            if i % config["iter_to_average"] == 0:
                fastswa_optim.update(base_network)
                mics.update_batchnorm(fastswa_basenet, dset_loaders)
                fastswa_basenet.train(False)
                temp_acc = image_classification_test(dset_loaders, fastswa_basenet, test_10crop=prep_config["test_10crop"])
                best_acc = temp_acc if temp_acc > best_acc else temp_acc
                log_str = "Evaluate fast-swa at iter {} - precision {:.5f}".format(i, temp_acc)
                config["out_file"].write(log_str+"\n")
                config["out_file"].flush()
                print(log_str)

        # Testing
        if i % config["test_interval"] == config["test_interval"] - 1:
            if config["mean_teacher"]:
                ema_base_network.train(False)
                temp_acc = image_classification_test(dset_loaders, ema_base_network, test_10crop=prep_config["test_10crop"])
            else:
                base_network.train(False)
                temp_acc = image_classification_test(dset_loaders, base_network, test_10crop=prep_config["test_10crop"])
            if temp_acc > best_acc:
                best_acc = temp_acc
                if config["mean_teacher"]:
                    best_model = {"base_network": ema_base_network.state_dict(), "ad_net": ad_net.state_dict()}
                else:
                    best_model = {"base_network": base_network.state_dict(), "ad_net": ad_net.state_dict()}
            log_str = "iter: {:05d}, precision: {:.5f}".format(i, temp_acc)
            config["out_file"].write(log_str+"\n")
            config["out_file"].flush()
            print(log_str)

        if config["save_model"] and (i + 1) % config["snapshot_interval"] == 0:
            torch.save({"base_network": base_network.state_dict(),
                        "ad_net": ad_net.state_dict()}, osp.join(config["output_path"],"latest_model.pth"))

    if config["save_model"]:
        torch.save(best_model, osp.join(config["output_path"], "best_model.pth"))
    config["out_file"].write("\n Final best acc: {:05f} \n\n".format(best_acc))
    print("\n Final best acc: {:05f} \n\n".format(best_acc))
    return best_acc

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Consistency based domain adaptation')
    # General, model & method
    parser.add_argument('--seed', default=1, type=int)
    parser.add_argument('--method', type=str, default='BASELINE',
                        choices=['BASELINE', 'CDAN', 'DANN', 'TDAN', 'TDAN_tensorial_product', 'TDAN_argmax', 'TDAN_random'])
    parser.add_argument('--net', type=str, default='ResNet50',
                        choices=["ResNet18", "ResNet34", "ResNet50", "ResNet101", "ResNet152"])
    parser.add_argument('--entropy', default=False, action="store_true",
                                    help="Add entropy weighting (as in CDAN)")
    parser.add_argument('--confidence', default=False, action="store_true",
                                    help="Add confidence loss to avoid confident predictions on target")
    # Training
    parser.add_argument('--num_iterations', default=20000, type=int)
    parser.add_argument('--test_interval', type=int, default=1000,
                        help="interval of two continuous test phase")
    parser.add_argument('--resume', type=str, default=None,
                        help="Path to .pth model")
    parser.add_argument('--save_model', action='store_true', default=False)
    parser.add_argument('--train_classifier', action='store_true', default=False,
                            help='Train classfier on top of a frozen feature extractor')
    parser.add_argument('--train_classifier_on_target', action='store_true', default=False,
                            help='Train classfier on target too.')
    # Dataset
    parser.add_argument('--dset', type=str, default='office',
                        choices=['office', 'image-clef', 'visda', 'office-home'],
                        help="The dataset or source dataset used")
    parser.add_argument('--s_dset_path', type=str, help="The source dataset path list")
    parser.add_argument('--t_dset_path', type=str, help="The target dataset path list")
    
    # Saving
    parser.add_argument('--snapshot_interval', type=int, default=5000,
                        help="interval of two continuous output model")
    parser.add_argument('--output_dir', type=str,
                        help="output directory of our model (in ../snapshot directory)")
    parser.add_argument('--lr', type=float, default=0.001, help="Learning rate")
    parser.add_argument('--batch_size', default=36, type=int, help='Batch size')

    # Loss trade-offs
    parser.add_argument('--lambda_transfer', default=1.0, type=float,
                                help='Loss trade-off for transfer term')
    parser.add_argument('--lambda_consistency', default=10.0, type=float,
                                help='Loss trade-off for consistency term')
    parser.add_argument('--lambda_confidence', default=0.4, type=float,
                                help='Loss trade-off for confidence term')

    # Consistency args
    parser.add_argument('--aug_severity', default=1, type=int,
                        help='Severity of base augmentation operators')
    parser.add_argument('--num_augs', default=1, type=int,
                        help='Number of augmentations to apply')
    parser.add_argument('--consistency', action='store_true', default=False,
                            help='Use consistency loss')
    parser.add_argument('--possible_augs', default=-1, type=int,
                            help='Size of the set of augmentations to sample from (-1: all of 13 augs)')
    parser.add_argument('--ramp_up', default=3500, type=int,
                            help="Consistency loss rampup periode.")
    parser.add_argument('--mean_teacher', action='store_true', default=False,
                            help='Use a teacher model to generated the target predictions')
    parser.add_argument('--use_source_labels', action='store_true', default=True,
                            help='Use source labels for TDAN.')
    parser.add_argument('--consistency_on_domain', action='store_true', default=False,
                            help='Use consistency on domain discriminator.')
    parser.add_argument('--vat', action='store_true', default=False)

    # Ablation
    parser.add_argument('--sensitivity', action='store_true', default=False)
    parser.add_argument('--label_smooth', action='store_true', default=False)
    parser.add_argument('--smooth_value', default=0.1, type=float) 
    parser.add_argument('--one_hot_target', action='store_true', default=False)

    # Stochastic weight averaging
    parser.add_argument('--iter_start_swa', default=None, type=int)
    parser.add_argument('--iters_per_cycle', default=None, type=int)
    parser.add_argument('--fast_swa', action='store_true', default=False)
    parser.add_argument('--iter_to_average', default=None, type=int)

    args = parser.parse_args()

    # Fix cuda, torch & numpy random seeds
    torch.backends.cudnn.benchmark = True
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    torch.cuda.manual_seed(args.seed)

    # Train config
    config = {}
    config["resume"] = args.resume
    config["train_classifier"] = args.train_classifier
    config["train_classifier_on_target"] = args.train_classifier_on_target
    if config["train_classifier_on_target"]:
        assert config["train_classifier"], "we train on target only for analysis"
    if config["train_classifier"]:
        assert config["resume"] is not None, "needs a pretrained feature extractor."
        args.consistency = False
        args.confidence = False
    config['method'] = args.method
    config["num_iterations"] = args.num_iterations
    config["test_interval"] = args.test_interval
    config["sensitivity"] = args.sensitivity
    config["vat"] = args.vat
    config["label_smooth"] = args.label_smooth
    config["smooth_value"] = args.smooth_value
    config["one_hot_target"] = args.one_hot_target
    config["save_model"] = args.save_model

    # Network Configs
    config["network"] = {"name": network.ResNetFc,
                         "params": {"resnet_name": args.net, "use_bottleneck": True,
                                    "bottleneck_dim": 256, "new_cls": True}}

    # Optimizer
    config["optimizer"] = {"type": optim.SGD,
                            "optim_params": {'lr': args.lr, "momentum": 0.9,
                                                    "weight_decay": 0.0005, "nesterov": True},             
                            "lr_type": "inv",
                            "lr_param": {"lr": args.lr, "gamma": 0.001, "power": 0.75,
                            "fast_swa": args.fast_swa, "iters_per_cycle": args.iters_per_cycle, "iter_start_swa": args.iter_start_swa}}
    config["fast_swa"] = args.fast_swa
    config["iter_to_average"] = args.iter_to_average

    # Dataset configs
    config["dataset"] = args.dset
    config["data"] = {"source": {"list_path": args.s_dset_path, "batch_size": args.batch_size},
                      "target": {"list_path": args.t_dset_path, "batch_size": args.batch_size},
                      "test": {"list_path": args.t_dset_path, "batch_size": 4}}

    # Preprocessing Configs
    config["prep"] = {"test_10crop": True,
                        'params': {"resize_size": 256, "crop_size": 224}}

    # Loss config
    config["loss"] = {"lambda_transfer": args.lambda_transfer,
                       "lambda_consistency": args.lambda_consistency,
                       "ramp_up": args.ramp_up, 
                       "entropy": args.entropy, 
                       "confidence": args.confidence, 
                       "consistency": args.consistency, 
                       "use_source_labels": args.use_source_labels, 
                       "consistency_on_domain": args.consistency_on_domain}

    # Consistency
    config["consistency"] = {"aug_severity": args.aug_severity, "num_augs": args.num_augs,
                                "possible_augs": args.possible_augs}
    config["mean_teacher"] = args.mean_teacher
    if config["mean_teacher"]:
        assert config["consistency"], "mean teacher works only with consistency"

    # Learning Rates per dataset (CDAN optimal parameters)
    if config["dataset"] == "office":
        if ("amazon" in args.s_dset_path and "webcam" in args.t_dset_path) or \
           ("webcam" in args.s_dset_path and "dslr" in args.t_dset_path) or \
           ("webcam" in args.s_dset_path and "amazon" in args.t_dset_path) or \
           ("dslr" in args.s_dset_path and "amazon" in args.t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        elif ("amazon" in args.s_dset_path and "dslr" in args.t_dset_path) or \
             ("dslr" in args.s_dset_path and "webcam" in args.t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.0003 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 31

    elif config["dataset"] == "image-clef":
        config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 12

    elif config["dataset"] == "visda":
        config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 12
        config["data"]["test"]["batch_size"] = 32

    elif config["dataset"] == "office-home":
        config["optimizer"]["lr_param"]["lr"] = 0.001 * (args.batch_size / 36)
        config["network"]["params"]["class_num"] = 65

    else:
        raise ValueError(
            'Dataset cannot be recognized. Please define your own dataset here.')

    # Saving path & Checkpoints
    config["output_for_test"] = True
    config["snapshot_interval"] = args.snapshot_interval
    config["output_path"] = "snapshot/" + args.output_dir
    if not osp.exists(config["output_path"]):
        os.system('mkdir -p '+config["output_path"])
    if not osp.exists(config["output_path"]):
        os.mkdir(config["output_path"])

    # Log file & Saving current config
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(config)
    config["out_file"] = open(osp.join(config["output_path"], "log.txt"), "w")
    config["out_file"].write(str(config)+"\n")
    config["out_file"].flush()

    # Train
    train(config)
