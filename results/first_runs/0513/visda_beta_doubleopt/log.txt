{'resume': None, 'train_classifier': False, 'train_classifier_on_target': False, 'data_augmentation': False, 'sensitivity': False, 'vat': False, 'method': 'TDAN', 'num_iterations': 40000, 'test_interval': 4000, 'compute_beta': False, 'two_classifiers': False, 'tdan_gs': False, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': False, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': 'data/visda-2017/train_list.txt', 'batch_size': 36}, 'target': {'list_path': 'data/visda-2017/validation_list.txt', 'batch_size': 36}, 'test': {'list_path': 'data/visda-2017/validation_list.txt', 'batch_size': 32}}, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224}}, 'loss': {'lambda_transfer': 1.0, 'lambda_consistency': 10.0, 'ramp_up': 0.2, 'entropy_weight': False, 'mse_weight': False, 'confidence': False, 'consistency': True, 'use_source_labels': True, 'consistency_on_domain': False, 'teacher_temperature': 1.0, 'random_percentage': 0.1}, 'detach_features': False, 'detach_features_gs': False, 'consistency': {'aug_severity': 1, 'num_augs': 4, 'possible_augs': -1}, 'mean_teacher': False, 'output_for_test': True, 'snapshot_interval': 5000, 'output_path': 'snapshot/visda_beta_doubleopt', 'out_file': <_io.TextIOWrapper name='snapshot/visda_beta_doubleopt/log.txt' mode='w' encoding='UTF-8'>}
iter: 00999, precision: 0.59581
iter: 01999, precision: 0.64673
iter: 02999, precision: 0.65099
iter: 03999, precision: 0.66845
iter: 04999, precision: 0.66630
iter: 05999, precision: 0.65368
iter: 06999, precision: 0.66161
iter: 07999, precision: 0.66567
iter: 08999, precision: 0.66874
iter: 09999, precision: 0.66094
iter: 10999, precision: 0.65947
iter: 11999, precision: 0.66325
iter: 12999, precision: 0.66018
iter: 13999, precision: 0.64796
iter: 14999, precision: 0.65018
iter: 15999, precision: 0.65399
iter: 16999, precision: 0.64875
iter: 17999, precision: 0.66240
iter: 18999, precision: 0.65558
iter: 19999, precision: 0.65816
iter: 00199, precision_gs: 0.65680, precision_gt: 0.67952, beta: 0.93401
iter: 00199, precision: 0.65455
iter: 00199, precision_gs: 0.65722, precision_gt: 0.67868, beta: 0.93756
iter: 00199, precision: 0.65659
iter: 00199, precision_gs: 0.65875, precision_gt: 0.68035, beta: 0.93691
iter: 00199, precision: 0.66303
iter: 00199, precision_gs: 0.66491, precision_gt: 0.67878, beta: 0.95874
iter: 00199, precision: 0.65386
iter: 00199, precision_gs: 0.65662, precision_gt: 0.68403, beta: 0.92042
iter: 00199, precision: 0.65686
iter: 00199, precision_gs: 0.65985, precision_gt: 0.67549, beta: 0.95417
iter: 00199, precision: 0.64911
iter: 00199, precision_gs: 0.65220, precision_gt: 0.68125, beta: 0.91672
iter: 00199, precision: 0.65370
iter: 00199, precision_gs: 0.65615, precision_gt: 0.67928, beta: 0.93293
iter: 00199, precision: 0.65267
iter: 00199, precision_gs: 0.65565, precision_gt: 0.68549, beta: 0.91358
iter: 00199, precision: 0.65803
iter: 00199, precision_gs: 0.66065, precision_gt: 0.68138, beta: 0.93910
iter: 00199, precision: 0.65491

 Final best acc: 0.668737 

