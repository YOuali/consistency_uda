{'resume': None, 'train_classifier': False, 'train_classifier_on_target': False, 'data_augmentation': False, 'sensitivity': False, 'vat': False, 'method': 'TDAN', 'num_iterations': 10000, 'test_interval': 500, 'compute_beta': False, 'two_classifiers': False, 'tdan_gs': False, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': False, 'class_num': 31}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'office', 'data': {'source': {'list_path': 'data/office/dslr_list.txt', 'batch_size': 36}, 'target': {'list_path': 'data/office/amazon_list.txt', 'batch_size': 36}, 'test': {'list_path': 'data/office/amazon_list.txt', 'batch_size': 4}}, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224}}, 'loss': {'lambda_transfer': 1.0, 'lambda_consistency': 10.0, 'ramp_up': 0.35, 'entropy_weight': False, 'mse_weight': False, 'confidence': False, 'consistency': True, 'use_source_labels': True, 'consistency_on_domain': False, 'teacher_temperature': 1.0, 'random_percentage': 0.1}, 'detach_features': False, 'detach_features_gs': False, 'consistency': {'aug_severity': 1, 'num_augs': 4, 'possible_augs': -1}, 'mean_teacher': False, 'output_for_test': True, 'snapshot_interval': 5000, 'output_path': 'snapshot/da_beta_doubleopt', 'out_file': <_io.TextIOWrapper name='snapshot/da_beta_doubleopt/log.txt' mode='w' encoding='UTF-8'>}
iter: 00999, precision: 0.65850
iter: 01999, precision: 0.69116
iter: 02999, precision: 0.71494
iter: 03999, precision: 0.71459
iter: 04999, precision: 0.71282
iter: 00049, precision_gs: 0.68548, precision_gt: 0.68797, beta: 0.99212
iter: 00049, precision: 0.69010
iter: 00049, precision_gs: 0.68974, precision_gt: 0.69152, beta: 0.99430
iter: 00049, precision: 0.68726
iter: 00049, precision_gs: 0.68761, precision_gt: 0.69187, beta: 0.98641
iter: 00049, precision: 0.68903
iter: 00049, precision_gs: 0.68903, precision_gt: 0.68655, beta: 1.00797
iter: 00049, precision: 0.68832
iter: 00049, precision_gs: 0.68903, precision_gt: 0.69116, beta: 0.99317
iter: 00049, precision: 0.68690
iter: 00049, precision_gs: 0.68726, precision_gt: 0.69116, beta: 0.98755
iter: 00049, precision: 0.69116
iter: 00049, precision_gs: 0.69081, precision_gt: 0.69081, beta: 1.00000
iter: 00049, precision: 0.68797
iter: 00049, precision_gs: 0.68939, precision_gt: 0.68761, beta: 1.00570
iter: 00049, precision: 0.68797
iter: 00049, precision_gs: 0.68868, precision_gt: 0.68974, beta: 0.99659
iter: 00049, precision: 0.69081
iter: 00049, precision_gs: 0.69081, precision_gt: 0.69116, beta: 0.99886
iter: 00049, precision: 0.68797

 Final best acc: 0.714945 

