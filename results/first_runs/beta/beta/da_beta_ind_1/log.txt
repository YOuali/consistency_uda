{'resume': None, 'train_classifier': False, 'train_classifier_on_target': False, 'data_augmentation': False, 'sensitivity': False, 'vat': False, 'method': 'TDAN', 'num_iterations': 20000, 'test_interval': 500, 'compute_beta': True, 'two_classifiers': True, 'tdan_gs': False, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 31}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'office', 'data': {'source': {'list_path': 'data/office/dslr_list.txt', 'batch_size': 36}, 'target': {'list_path': 'data/office/amazon_list.txt', 'batch_size': 36}, 'test': {'list_path': 'data/office/amazon_list.txt', 'batch_size': 4}}, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224}}, 'loss': {'lambda_transfer': 1.0, 'lambda_consistency': 10.0, 'ramp_up': 0.35, 'entropy_weight': False, 'mse_weight': False, 'confidence': False, 'consistency': True, 'use_source_labels': True, 'consistency_on_domain': False, 'teacher_temperature': 1.0, 'random_percentage': 0.1}, 'detach_features': True, 'detach_features_gs': False, 'consistency': {'aug_severity': 1, 'num_augs': 4, 'possible_augs': -1}, 'mean_teacher': False, 'output_for_test': True, 'snapshot_interval': 5000, 'output_path': 'snapshot/da_beta_ind_1', 'out_file': <_io.TextIOWrapper name='snapshot/da_beta_ind_1/log.txt' mode='w' encoding='UTF-8'>}
iter: 00499, precision: 0.63330, beta: 1.02170
iter: 00999, precision: 0.67838, beta: 1.00888
iter: 01499, precision: 0.69116, beta: 0.99317
iter: 01999, precision: 0.69329, beta: 1.04459
iter: 02499, precision: 0.71104, beta: 1.01114
iter: 02999, precision: 0.71282, beta: 1.04104
iter: 03499, precision: 0.71814, beta: 1.02049
iter: 03999, precision: 0.71707, beta: 1.02172
iter: 04499, precision: 0.72559, beta: 1.00518
iter: 04999, precision: 0.72843, beta: 1.00788
iter: 05499, precision: 0.73056, beta: 0.98704
iter: 05999, precision: 0.73127, beta: 0.98701
iter: 06499, precision: 0.73482, beta: 0.97276
iter: 06999, precision: 0.72914, beta: 0.98331
iter: 07499, precision: 0.72843, beta: 0.99482
iter: 07999, precision: 0.72701, beta: 1.00260
iter: 08499, precision: 0.73589, beta: 0.96761
iter: 08999, precision: 0.73589, beta: 0.97265
iter: 09499, precision: 0.73482, beta: 0.95056
iter: 09999, precision: 0.73269, beta: 0.96551
iter: 10499, precision: 0.73660, beta: 0.96627
iter: 10999, precision: 0.73482, beta: 0.97024
iter: 11499, precision: 0.73411, beta: 0.97789
iter: 11999, precision: 0.73340, beta: 0.98049
iter: 12499, precision: 0.73305, beta: 0.97293
iter: 12999, precision: 0.73553, beta: 0.97395
iter: 13499, precision: 0.73589, beta: 0.98679
iter: 13999, precision: 0.73482, beta: 0.98554
iter: 14499, precision: 0.73305, beta: 0.98307
iter: 14999, precision: 0.73979, beta: 0.99594
iter: 15499, precision: 0.73802, beta: 0.98406
iter: 15999, precision: 0.73376, beta: 0.99867
iter: 16499, precision: 0.74086, beta: 0.98920
iter: 16999, precision: 0.74157, beta: 0.97727
iter: 17499, precision: 0.73944, beta: 0.98397
iter: 17999, precision: 0.74547, beta: 0.97297
iter: 18499, precision: 0.74015, beta: 0.97609
iter: 18999, precision: 0.74157, beta: 0.98252
iter: 19499, precision: 0.73944, beta: 0.97229
iter: 19999, precision: 0.73873, beta: 0.97364

 Final best acc: 0.745474 

