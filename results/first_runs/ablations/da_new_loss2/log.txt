{'resume': None, 'train_classifier': False, 'train_classifier_on_target': False, 'data_augmentation': False, 'sensitivity': False, 'vat': False, 'method': 'TDAN', 'num_iterations': 20000, 'test_interval': 500, 'compute_beta': False, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'class_num': 31}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'office', 'data': {'source': {'list_path': 'data/office/dslr_list.txt', 'batch_size': 36}, 'target': {'list_path': 'data/office/amazon_list.txt', 'batch_size': 36}, 'test': {'list_path': 'data/office/amazon_list.txt', 'batch_size': 4}}, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224}}, 'loss': {'lambda_transfer': 1.0, 'lambda_consistency': 1.0, 'ramp_up': 0.35, 'entropy_weight': False, 'mse_weight': False, 'confidence': False, 'consistency': True, 'use_source_labels': True, 'consistency_on_domain': False, 'teacher_temperature': 1.0, 'random_percentage': 0.1}, 'detach_features': False, 'consistency': {'aug_severity': 1, 'num_augs': 4, 'possible_augs': -1}, 'mean_teacher': False, 'output_for_test': True, 'snapshot_interval': 5000, 'output_path': 'snapshot/da_new_loss2', 'out_file': <_io.TextIOWrapper name='snapshot/da_new_loss2/log.txt' mode='w' encoding='UTF-8'>}
iter: 00499, precision: 0.63969
iter: 00999, precision: 0.67164
iter: 01499, precision: 0.69933
iter: 01999, precision: 0.71565
iter: 02499, precision: 0.72133
iter: 02999, precision: 0.74015
iter: 03499, precision: 0.74228
iter: 03999, precision: 0.74050
iter: 04499, precision: 0.74370
iter: 04999, precision: 0.73979
iter: 05499, precision: 0.74760
iter: 05999, precision: 0.74547
iter: 06499, precision: 0.75222
iter: 06999, precision: 0.75151
iter: 07499, precision: 0.74973
iter: 07999, precision: 0.74902
iter: 08499, precision: 0.74831
iter: 08999, precision: 0.74973
iter: 09499, precision: 0.75222
iter: 09999, precision: 0.74973
iter: 10499, precision: 0.74973
iter: 10999, precision: 0.75009
iter: 11499, precision: 0.74760
iter: 11999, precision: 0.74654
iter: 12499, precision: 0.74583
iter: 12999, precision: 0.74725
iter: 13499, precision: 0.74618
iter: 13999, precision: 0.74654
iter: 14499, precision: 0.74512
iter: 14999, precision: 0.74583
iter: 15499, precision: 0.74405
iter: 15999, precision: 0.74441
iter: 16499, precision: 0.74405
iter: 16999, precision: 0.74334
iter: 17499, precision: 0.74547
iter: 17999, precision: 0.74512
iter: 18499, precision: 0.74547
iter: 18999, precision: 0.74476
iter: 19499, precision: 0.74192
iter: 19999, precision: 0.74157

 Final best acc: 0.752219 

