{'resume': None, 'train_classifier': False, 'train_classifier_on_target': False, 'data_augmentation': False, 'sensitivity': False, 'vat': False, 'method': 'TDAN', 'num_iterations': 40000, 'test_interval': 4000, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'compute_beta': False, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': 'data/visda-2017/train_list.txt', 'batch_size': 36}, 'target': {'list_path': 'data/visda-2017/validation_list.txt', 'batch_size': 36}, 'test': {'list_path': 'data/visda-2017/validation_list.txt', 'batch_size': 32}}, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224}}, 'loss': {'lambda_transfer': 1.0, 'lambda_consistency': 0.1, 'ramp_up': 0.2, 'entropy': False, 'confidence': False, 'consistency': True, 'use_source_labels': True, 'consistency_on_domain': False, 'teacher_temperature': 1.0, 'random_percentage': 0.1}, 'detach_features': False, 'consistency': {'aug_severity': 1, 'num_augs': 4, 'possible_augs': -1}, 'mean_teacher': False, 'output_for_test': True, 'snapshot_interval': 5000, 'output_path': 'snapshot/visda_new_loss_seed1', 'out_file': <_io.TextIOWrapper name='snapshot/visda_new_loss_seed1/log.txt' mode='w' encoding='UTF-8'>}
iter: 03999, precision: 0.66679
iter: 07999, precision: 0.68213
iter: 11999, precision: 0.69412
iter: 15999, precision: 0.68764
iter: 19999, precision: 0.70624
iter: 23999, precision: 0.69602
iter: 27999, precision: 0.69455
iter: 31999, precision: 0.70631
iter: 35999, precision: 0.69860
iter: 39999, precision: 0.69741

 Final best acc: 0.706308 

