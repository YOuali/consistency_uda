import numpy as np

"""
Learning rate scheduler (as in CDAN)
"""

def cosine_rampdown(current, rampdown_length):
    assert 0 <= current <= rampdown_length
    return max(0., float(.5 * (np.cos(np.pi * current / rampdown_length) + 1)))

def inv_lr_scheduler(optimizer, iter_num, gamma, power, lr, weight_decay=0.0005,
                            iter_start_swa=None, iters_per_cycle=None, fast_swa=False):
    """Decay learning rate by a factor of 0.1 every lr_decay_epoch epochs."""
    if (not fast_swa) or iter_num < iter_start_swa:
        lr = lr * (1 + gamma * iter_num) ** (-power)
    else:
        lr = lr * 2. *  cosine_rampdown((iter_start_swa - iters_per_cycle) + ((iter_num - iter_start_swa) % iters_per_cycle), \
                                                (iter_start_swa+iters_per_cycle))

    for param_group in optimizer.param_groups:
        param_group['lr'] = lr * param_group['lr_mult']
        param_group['weight_decay'] = weight_decay * param_group['decay_mult']

    return optimizer

schedule_dict = {"inv": inv_lr_scheduler}

"""
SWA or fastSWA
"""

class WeightSWA (object):
    def __init__(self, swa_model):
        self.num_params = 0
        self.swa_model = swa_model

    def update(self, student_model):
        self.num_params += 1
        print("Updating SWA. Current num_params =", self.num_params)
        if self.num_params == 1:
            print("Loading State Dict")
            self.swa_model.load_state_dict(student_model.state_dict())
        else:
            inv = 1./float(self.num_params)
            for swa_p, src_p in zip(self.swa_model.parameters(), student_model.parameters()):
                swa_p.data.add_(-inv*swa_p.data)
                swa_p.data.add_(inv*src_p.data)
    
    def reset(self):
        self.num_params = 0

"""
Ramp-up functions for consistency loss
"""

def sigmoid_rampup(current, rampup_length):
    if rampup_length == 0:
        return 1.0
    current = np.clip(current, 0.0, rampup_length)
    phase = 1.0 - current / rampup_length
    return float(np.exp(-5.0 * phase * phase))

def log_rampup(current, rampup_length):
    if rampup_length == 0:
        return 1.0
    current = np.clip(current, 0.0, rampup_length)
    return float(1 - np.exp(-5.0 * current / rampup_length))

"""
EMA (exponential moving average) updates
"""

def update_ema_variables(model, ema_model, global_step, alpha=0.99):
    # ref https://arxiv.org/abs/1703.01780
    # Use the true average until the exponential average is more correct
    alpha = min(1 - 1 / (global_step + 1), alpha)
    for ema_param, param in zip(ema_model.parameters(), model.parameters()):
        ema_param.data.mul_(alpha).add_(1 - alpha, param.data)

"""
AverageMeter for printing acc and losses 
"""

class AverageMeter(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

"""
Measures of discriminability as in http://proceedings.mlr.press/v97/chen19i/chen19i.pdf
"""

def sigmas(features_s, features_t, normalize=True, k=None):
    _, s_s, _ = torch.svd(features_s)
    _, s_t, _ = torch.svd(features_t)
    if normalize:
        s_s = s_s/s_s.max()
        s_t = s_t/s_t.max()
    if k is not None:
        s_s, s_t = s_s[:k], s_t[:k]
    return s_s, s_t


def linear_discriminant_analysis(features, labels):
    mean_all = features.mean(0).unsqueeze(0)
    B, N = features.shape
    s_b, s_w = torch.zeros(N, N), torch.zeros(N, N)
    for i in labels.unique():
        idx = (labels == i)
        f = features[idx]
        f_mean = f.mean(0).unsqueeze(0)
        s_b += idx.sum() * ((f_mean - mean_all).t() @ (f_mean - mean_all))
        s_w += (f - f_mean).t()  @ (f - f_mean)
    
    s = torch.pinverse(s_w) @ s_b
    w, _, _ = torch.svd(s)

    numerator = w.t() @ s_b @ w
    denominator = w.t() @ s_w @ w
    max_j_w = torch.diag(numerator).sum() / torch.diag(denominator).sum()
    return max_j_w


def corr_angles(features_s, features_t, normalize=True, k=None):
    u_s, s_s, v_s = torch.svd(features_s)
    u_t, s_t, v_t = torch.svd(features_t)

    u_s_norm = torch.norm(u_s, dim=1)
    u_t_norm = torch.norm(u_t, dim=1)

    cos_psi = ((u_t * u_s).sum(1)) / (u_s_norm * u_t_norm)

    if normalize:
        cos_psi = cos_psi/cos_psi.max()
    if k is not None:
        cos_psi = cos_psi[:k]
    return cos_psi

