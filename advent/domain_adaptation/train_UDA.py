# --------------------------------------------------------
# Domain adpatation training
# Copyright (c) 2019 valeo.ai
#
# Written by Tuan-Hung Vu
# --------------------------------------------------------
import os
import sys
from pathlib import Path

import os.path as osp
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch import nn
from torchvision.utils import make_grid
from tqdm import tqdm

from model.discriminator import get_fc_discriminator
from utils.func import adjust_learning_rate, adjust_learning_rate_discriminator
from utils.func import loss_calc, bce_loss
from utils.loss import sigmoid_rampup, softmax_mse_loss, AverageMeter
from utils.func import prob_2_entropy
from utils.viz_segmask import colorize_mask
from model.deeplabv2 import VAT

def train_domain_adaptation(model, trainloader, targetloader, cfg):
    ''' UDA training with advent
    '''
    # Create the model and start the training.
    input_size_source = cfg.TRAIN.INPUT_SIZE_SOURCE
    input_size_target = cfg.TRAIN.INPUT_SIZE_TARGET
    device = cfg.GPU_ID
    num_classes = cfg.NUM_CLASSES
    viz_tensorboard = os.path.exists(cfg.TRAIN.TENSORBOARD_LOGDIR)
    if viz_tensorboard:
        writer = SummaryWriter(log_dir=cfg.TRAIN.TENSORBOARD_LOGDIR)

    # SEGMNETATION NETWORK
    model.train()
    model.to(device)
    cudnn.benchmark = True
    cudnn.enabled = True

    # DISCRIMINATOR NETWORK
    # feature-level
    d_aux = get_fc_discriminator(num_classes=num_classes, tdan=cfg.TRAIN.TDAN)
    d_aux.train()
    d_aux.to(device)

    # seg maps, i.e. output, level
    d_main = get_fc_discriminator(num_classes=num_classes, tdan=cfg.TRAIN.TDAN)
    d_main.train()
    d_main.to(device)

    # OPTIMIZERS
    # segnet's optimizer
    optimizer = optim.SGD(model.optim_parameters(cfg.TRAIN.LEARNING_RATE),
                          lr=cfg.TRAIN.LEARNING_RATE,
                          momentum=cfg.TRAIN.MOMENTUM,
                          weight_decay=cfg.TRAIN.WEIGHT_DECAY)

    # discriminators' optimizers
    optimizer_d_aux = optim.Adam(d_aux.parameters(), lr=cfg.TRAIN.LEARNING_RATE_D,
                                 betas=(0.9, 0.99))
    optimizer_d_main = optim.Adam(d_main.parameters(), lr=cfg.TRAIN.LEARNING_RATE_D,
                                  betas=(0.9, 0.99))

    # interpolate output segmaps
    interp = nn.Upsample(size=(input_size_source[1], input_size_source[0]), mode='bilinear',
                         align_corners=True)
    interp_target = nn.Upsample(size=(input_size_target[1], input_size_target[0]), mode='bilinear',
                                align_corners=True)

    # labels for adversarial training
    source_label = 0
    target_label = 1
    trainloader_iter = enumerate(trainloader)
    targetloader_iter = enumerate(targetloader)
    
    # for consistency tranining
    ramp_up_end = int(cfg.TRAIN.RAMP_UP * cfg.TRAIN.EARLY_STOP)
    if cfg.TRAIN.VAT:
        get_adv_perturbation = VAT(model)

    avg_Lseg_aux = AverageMeter()
    avg_Lseg_main = AverageMeter()
    avg_Ladv_Taux = AverageMeter()
    avg_Ladv_Tmain = AverageMeter()
    avg_Ld_aux = AverageMeter()
    avg_Ld_main = AverageMeter()
    avg_Lvat = AverageMeter()
    avg_Lconst = AverageMeter()

    for i_iter in tqdm(range(cfg.TRAIN.EARLY_STOP + 1)):

        # reset optimizers
        optimizer.zero_grad()
        optimizer_d_aux.zero_grad()
        optimizer_d_main.zero_grad()
    
        # adapt LR if needed
        adjust_learning_rate(optimizer, i_iter, cfg)
        adjust_learning_rate_discriminator(optimizer_d_aux, i_iter, cfg)
        adjust_learning_rate_discriminator(optimizer_d_main, i_iter, cfg)

        # UDA Training
        # only train segnet. Don't accumulate grads in disciminators
        for param in d_aux.parameters():
            param.requires_grad = False
        for param in d_main.parameters():
            param.requires_grad = False
        
        # train on source
        _, batch = trainloader_iter.__next__()
        images_source, labels, _, _ = batch
        pred_src_aux_small, pred_src_main_small = model(images_source.cuda(device))
        if cfg.TRAIN.MULTI_LEVEL:
            pred_src_aux = interp(pred_src_aux_small)
            loss_seg_src_aux = loss_calc(pred_src_aux, labels, device)
        else:
            loss_seg_src_aux = 0
        pred_src_main = interp(pred_src_main_small)
        loss_seg_src_main = loss_calc(pred_src_main, labels, device)
        loss = (cfg.TRAIN.LAMBDA_SEG_MAIN * loss_seg_src_main + cfg.TRAIN.LAMBDA_SEG_AUX * loss_seg_src_aux)
        loss.backward()

        # adversarial training to fool the discriminator
        _, batch = targetloader_iter.__next__()
        if cfg.TRAIN.CONSISTENCY:
            images, images_aug, _, _, _ = batch
        else:
            images, _, _, _ = batch

        pred_trg_aux_small, pred_trg_main_small = model(images.cuda(device))
        if cfg.TRAIN.MULTI_LEVEL:
            pred_trg_aux = interp_target(pred_trg_aux_small)
            d_out_aux = d_aux(prob_2_entropy(F.softmax(pred_trg_aux, dim=1))) if cfg.TRAIN.ENTROPY else d_aux(F.softmax(pred_trg_aux, dim=1))
            loss_adv_trg_aux = bce_loss(d_out_aux, source_label, cfg.TRAIN.TDAN)
            if cfg.TRAIN.TDAN:
                pred_trg_aux_resized = F.interpolate(pred_trg_aux_small, mode="bilinear",
                                        size=(d_out_aux.size(2), d_out_aux.size(3)), align_corners=True)
                loss_adv_trg_aux = (pred_trg_aux_resized.softmax(1).detach() * loss_adv_trg_aux).mean(dim=(0,2,3)).sum()
        else:
            loss_adv_trg_aux = 0
        pred_trg_main = interp_target(pred_trg_main_small)
        d_out_main = d_main(prob_2_entropy(F.softmax(pred_trg_main, dim=1))) if cfg.TRAIN.ENTROPY else d_main(F.softmax(pred_trg_main, dim=1))
        loss_adv_trg_main = bce_loss(d_out_main, source_label, cfg.TRAIN.TDAN)
        if cfg.TRAIN.TDAN:
            pred_trg_main_resized = F.interpolate(pred_trg_main_small, mode="bilinear",
                                    size=(d_out_main.size(2), d_out_main.size(3)), align_corners=True)
            loss_adv_trg_main = (pred_trg_main_resized.softmax(1).detach() * loss_adv_trg_main).mean(dim=(0,2,3)).sum()

        loss = (cfg.TRAIN.LAMBDA_ADV_MAIN * loss_adv_trg_main
                + cfg.TRAIN.LAMBDA_ADV_AUX * loss_adv_trg_aux)

        # Consistency loss
        if cfg.TRAIN.CONSISTENCY:
            pred_trg_aux_aug, pred_trg_main_aug = model(images_aug.cuda(device))
            if cfg.TRAIN.MULTI_LEVEL:
                loss_consistency_aux = softmax_mse_loss(pred_trg_aux_aug, pred_trg_aux_small.detach())
            else:
                loss_consistency_aux = 0
            loss_consistency_main = softmax_mse_loss(pred_trg_main_aug, pred_trg_main_small.detach())
            loss_consistency = (loss_consistency_main + loss_consistency_aux / 5.) 
            loss_consistency = loss_consistency * sigmoid_rampup(i_iter, ramp_up_end) * cfg.TRAIN.LAMBDA_CONSISTENCY
            loss = loss + loss_consistency
        else:
            loss_consistency = 0

        # VAT loss
        if cfg.TRAIN.VAT:
            images_vat = get_adv_perturbation(images.cuda(device)) + images.cuda(device)
            pred_trg_aux_vat, pred_trg_main_vat = model(images_vat)
            if cfg.TRAIN.MULTI_LEVEL:
                loss_vat_aux = softmax_mse_loss(pred_trg_aux_vat, pred_trg_aux_small.detach())
            else:
                loss_vat_aux = 0
            loss_vat_main = softmax_mse_loss(pred_trg_main_vat, pred_trg_main_small.detach())
            loss_vat = (loss_vat_main + loss_vat_aux / 5.) 
            loss_vat = loss_vat * sigmoid_rampup(i_iter, ramp_up_end) * cfg.TRAIN.LAMBDA_CONSISTENCY
            loss = loss + loss_vat / 3.
        else:
            loss_vat = 0

        loss = loss
        loss.backward()

        # Train discriminator networks
        # enable training mode on discriminator networks
        for param in d_aux.parameters():
            param.requires_grad = True
        for param in d_main.parameters():
            param.requires_grad = True
        
        # train with source
        pred_src_main = pred_src_main.detach()
        d_out_main = d_main(prob_2_entropy(F.softmax(pred_src_main, dim=1))) if cfg.TRAIN.ENTROPY else d_main(F.softmax(pred_src_main, dim=1))

        if cfg.TRAIN.TDAN:
	        source_labels_resized = F.interpolate(labels[None].to(d_out_main.device), mode="nearest", size=(d_out_main.size(2), d_out_main.size(3))).to(torch.int64)
	        source_labels_resized[source_labels_resized == cfg.TRAIN.IGNORE_LABEL] = 0.
	        source_labels_resized = F.one_hot(source_labels_resized[0], num_classes=num_classes).permute(0, 3, 1, 2).float()

        if cfg.TRAIN.MULTI_LEVEL:
            pred_src_aux = pred_src_aux.detach()
            d_out_aux = d_aux(prob_2_entropy(F.softmax(pred_src_aux, dim=1))) if cfg.TRAIN.ENTROPY else d_aux(F.softmax(pred_src_aux, dim=1))
            loss_d_aux = bce_loss(d_out_aux, source_label, cfg.TRAIN.TDAN)
            if cfg.TRAIN.TDAN:
                # pred_src_aux_resized = F.interpolate(pred_src_aux_small, mode="bilinear",
                #                         size=(d_out_aux.size(2), d_out_aux.size(3)), align_corners=True)
                # loss_d_aux = (pred_src_aux_resized.softmax(1).detach() * loss_d_aux).mean(dim=(0,2,3)).sum()
                loss_d_aux = (source_labels_resized * loss_d_aux).mean(dim=(0,2,3)).sum()
            loss_d_aux = loss_d_aux / 2
            loss_d_aux.backward()
        else:
            loss_d_aux = 0


        loss_d_main = bce_loss(d_out_main, source_label, cfg.TRAIN.TDAN)
        if cfg.TRAIN.TDAN:
            # pred_src_main_resized = F.interpolate(pred_src_main_small, mode="bilinear",
            #                         size=(d_out_main.size(2), d_out_main.size(3)), align_corners=True)
            # loss_d_main = (pred_src_main_resized.softmax(1).detach() * loss_d_main).mean(dim=(0,2,3)).sum()
            loss_d_main = (source_labels_resized * loss_d_main).mean(dim=(0,2,3)).sum()
        loss_d_main = loss_d_main / 2
        loss_d_main.backward()

        # train with target
        if cfg.TRAIN.MULTI_LEVEL:
            pred_trg_aux = pred_trg_aux.detach()
            d_out_aux = d_aux(prob_2_entropy(F.softmax(pred_trg_aux, dim=1))) if cfg.TRAIN.ENTROPY else d_aux(F.softmax(pred_trg_aux, dim=1))
            loss_d_aux = bce_loss(d_out_aux, target_label, cfg.TRAIN.TDAN)
            if cfg.TRAIN.TDAN:
                loss_d_aux = (pred_trg_aux_resized.softmax(1).detach() * loss_d_aux).mean(dim=(0,2,3)).sum()
            loss_d_aux = loss_d_aux / 2
            loss_d_aux.backward()
        else:
            loss_d_aux = 0
        
        pred_trg_main = pred_trg_main.detach()
        d_out_main = d_main(prob_2_entropy(F.softmax(pred_trg_main, dim=1))) if cfg.TRAIN.ENTROPY else d_main(F.softmax(pred_trg_main, dim=1))

        loss_d_main = bce_loss(d_out_main, target_label, cfg.TRAIN.TDAN)
        if cfg.TRAIN.TDAN:
            loss_d_main = (pred_trg_main_resized.softmax(1).detach() * loss_d_main).mean(dim=(0,2,3)).sum()

        loss_d_main = loss_d_main / 2
        loss_d_main.backward()

        optimizer.step()
        if cfg.TRAIN.MULTI_LEVEL:
            optimizer_d_aux.step()
        optimizer_d_main.step()

        avg_Lseg_aux.update(loss_seg_src_aux)
        avg_Lseg_main.update(loss_seg_src_main)
        avg_Ladv_Taux.update(loss_adv_trg_aux)
        avg_Ladv_Tmain.update(loss_adv_trg_main)
        avg_Ld_aux.update(loss_d_aux)
        avg_Ld_main.update(loss_d_main)
        avg_Lvat.update(loss_vat)
        avg_Lconst.update(loss_consistency)

        current_losses = {'Lseg_aux': avg_Lseg_aux.avg,
                          'Lseg_main': avg_Lseg_main.avg,
                          'Ladv_Taux': avg_Ladv_Taux.avg,
                          'Ladv_Tmain': avg_Ladv_Tmain.avg,
                          'Ld_aux': avg_Ld_aux.avg,
                          'Ld_main': avg_Ld_main.avg,
                          'Lvat': avg_Lvat.avg,
                          'Lconst': avg_Lconst.avg}

        print_losses(current_losses, i_iter)

        if i_iter % cfg.TRAIN.SAVE_PRED_EVERY == 0 and i_iter != 0:
            print('taking snapshot ...')
            print('exp =', cfg.TRAIN.SNAPSHOT_DIR)
            snapshot_dir = Path(cfg.TRAIN.SNAPSHOT_DIR)
            torch.save(model.state_dict(), snapshot_dir / f'model_{i_iter}.pth')
            # torch.save(d_aux.state_dict(), snapshot_dir / f'model_{i_iter}_D_aux.pth')
            # torch.save(d_main.state_dict(), snapshot_dir / f'model_{i_iter}_D_main.pth')

            avg_Lseg_aux.reset(); avg_Lseg_main.reset(); avg_Ladv_Taux.reset()
            avg_Ladv_Tmain.reset(); avg_Ld_aux.reset(); avg_Ld_main.reset()
            avg_Lvat.reset(); avg_Lconst.reset()

            if i_iter >= cfg.TRAIN.EARLY_STOP - 1:
                break
        sys.stdout.flush()

        # Visualize with tensorboard
        if viz_tensorboard:
            log_losses_tensorboard(writer, current_losses, i_iter)

            if i_iter % cfg.TRAIN.TENSORBOARD_VIZRATE == cfg.TRAIN.TENSORBOARD_VIZRATE - 1:
                draw_in_tensorboard(writer, images, i_iter, pred_trg_main, num_classes, 'T')
                draw_in_tensorboard(writer, images_source, i_iter, pred_src_main, num_classes, 'S')


def draw_in_tensorboard(writer, images, i_iter, pred_main, num_classes, type_):
    grid_image = make_grid(images[:3].clone().cpu().data, 3, normalize=True)
    writer.add_image(f'Image - {type_}', grid_image, i_iter)

    grid_image = make_grid(torch.from_numpy(np.array(colorize_mask(np.asarray(
        np.argmax(F.softmax(pred_main, dim=1).cpu().data[0].numpy().transpose(1, 2, 0),
                  axis=2), dtype=np.uint8)).convert('RGB')).transpose(2, 0, 1)), 3,
                           normalize=False, range=(0, 255))
    writer.add_image(f'Prediction - {type_}', grid_image, i_iter)

    output_sm = F.softmax(pred_main, dim=1).cpu().data[0].numpy().transpose(1, 2, 0)
    output_ent = np.sum(-np.multiply(output_sm, np.log2(output_sm)), axis=2,
                        keepdims=False)
    grid_image = make_grid(torch.from_numpy(output_ent), 3, normalize=True,
                           range=(0, np.log2(num_classes)))
    writer.add_image(f'Entropy - {type_}', grid_image, i_iter)


def print_losses(current_losses, i_iter):
    list_strings = []
    for loss_name, loss_value in current_losses.items():
        list_strings.append(f'{loss_name} = {to_numpy(loss_value):.3f} ')
    full_string = ' '.join(list_strings)
    tqdm.write(f'iter = {i_iter} {full_string}')


def log_losses_tensorboard(writer, current_losses, i_iter):
    for loss_name, loss_value in current_losses.items():
        writer.add_scalar(f'data/{loss_name}', to_numpy(loss_value), i_iter)

def to_numpy(tensor):
    if isinstance(tensor, (int, float)):
        return tensor
    else:
        return tensor.data.cpu().numpy()
