import numpy as np
import matplotlib.pyplot as plt
import scipy.io
import torch
import torch.nn as nn
import torch.nn.functional as F
import os.path
import numpy as np
from sklearn import datasets
from tqdm import tqdm
import matplotlib
import network
import loss
import pre_process as prep
from datasets import ImageList, ConsistencyDataset
import mics
from torch.utils.data import DataLoader
import numpy as np
import numexpr as ne
import argparse

parser = argparse.ArgumentParser(description='sensitivity')
parser.add_argument('--num_samples', default=None, type=int)
parser.add_argument('--task', default=None, type=str)
parser.add_argument('--method', default=None, type=str)
args = parser.parse_args()

task = args.task
num_samples = args.num_samples
method = args.method


matplotlib.rcParams['axes.spines.right'] = False
matplotlib.rcParams['axes.spines.top'] = False
matplotlib.rcParams['axes.spines.bottom'] = False
matplotlib.rc('figure', figsize=(7, 5))

def _get_zero_centered_circle(radius: float, num_samples: int) -> np.ndarray:
    rads = np.linspace(0, 2 * np.pi, num_samples, endpoint=False)
    circle = np.zeros([num_samples, 2])
    ne.evaluate('cos(rads)', out=circle[:, 0])
    ne.evaluate('sin(rads)', out=circle[:, 1])
    circle *= radius
    return circle

def _get_2d_points_fitting_transform(vertices: np.ndarray, scale: float) -> np.ndarray:
    angles = np.pi * np.linspace(0, 2, len(vertices), endpoint=False)
    angles = np.reshape(angles, (-1, 1))
    coords_2d = np.concatenate((np.cos(angles), np.sin(angles)), axis=1) * scale
    transform = np.dot(np.linalg.pinv(coords_2d), vertices)
    return transform


def get_fitted_ellipse_and_stats(num_samples: int, vertices: np.ndarray) -> np.ndarray:
    shape = vertices[0].shape if vertices.ndim > 1 else (1,)
    vertices = np.reshape(vertices, (vertices.shape[0], -1))

    center = np.mean(vertices, axis=0)
    circle = _get_zero_centered_circle(1, num_samples)

    transform = _get_2d_points_fitting_transform(vertices - center, scale=1)
    circle = np.dot(circle, transform)
    circle += center
    circle = np.reshape(circle, (num_samples,) + shape)
    return circle

webcam_path = "data/office/webcam_list.txt"
amazon_path = "data/office/amazon_list.txt"
dslr_path = "data/office/dslr_list.txt"
batch_size = num_samples

# Preprocessing Configs
prep_params = {"resize_size": 256, "crop_size": 224}
prep = prep.image_train(**prep_params)

webcam_dataset =  ImageList(open(webcam_path).readlines(), transform=prep)
amazon_dataset =  ImageList(open(amazon_path).readlines(), transform=prep)
dslr_dataset =  ImageList(open(dslr_path).readlines(), transform=prep)

loader_webcam = DataLoader(webcam_dataset, batch_size=batch_size, shuffle=True, num_workers=4, drop_last=True)
loader_amazon = DataLoader(amazon_dataset, batch_size=batch_size, shuffle=True, num_workers=4, drop_last=True)
loader_dslr = DataLoader(dslr_dataset, batch_size=batch_size, shuffle=True, num_workers=4, drop_last=True)

network_config = {"name": network.ResNetFc,
                     "params": {"resnet_name": "ResNet50", "use_bottleneck": True,
                                "bottleneck_dim": 256, "new_cls": True}}
network_config["params"]["class_num"] = 31
class_num = 31

model_paths = {
	"dann_ad":"snapshot/dann_ad1/latest_model.pth",
	"dann_da":"snapshot/dann_da1/latest_model.pth",
	"dann_dw":"snapshot/dann_dw1/latest_model.pth",
	"dann_wa":"snapshot/dann_wa1/latest_model.pth",
	"dann_aw":"snapshot/dann_aw1/latest_model.pth",
	"resnet_ad":"snapshot/resnet_ad1/latest_model.pth",
	"resnet_da":"snapshot/resnet_da1/latest_model.pth",
	"resnet_dw":"snapshot/resnet_dw1/latest_model.pth",
	"resnet_aw":"snapshot/resnet_aw1/latest_model.pth",
	"dann_wa":"snapshot/dann_wa1/latest_model.pth",
	"tdan_ad":"snapshot/tdan_ad1/latest_model.pth",
	"tdan_da":"snapshot/tdan_da1/latest_model.pth",
	"tdan_dw":"snapshot/tdan_dw1/latest_model.pth",
	"tdan_wa":"snapshot/tdan_wa1/latest_model.pth",
	"dann_aw":"snapshot/dann_aw1/latest_model.pth"
}

checkpoint_path = model_paths[f"{method}_{task}"]

def l2_normalize(d):
    d_reshaped = d.view(d.size(0), -1, *(1 for _ in range(d.dim() - 2)))
    d /= torch.norm(d_reshaped, dim=1, keepdim=True) + 1e-8
    return d

if task[1] == "a":
	loader_target = loader_amazon
elif task[1] == "d":
	loader_target = loader_dslr
elif task[1] == "w":
	loader_target = loader_webcam

def get_jacobian(x_batch, noutputs):
    global base_network
    jacobians = []
    for x in x_batch:
        base_network.zero_grad()
        x = x[None].cuda()
        x = x.repeat(noutputs, 1, 1, 1)
        x.requires_grad_(True)
        _, y = base_network(x)
        y = y.softmax(1)
        y.backward(torch.eye(noutputs).to(x.device))
        grad = x.grad.data.clone()
        jacobians.append(torch.norm(grad, p='fro'))
        x.grad.data.zero_()
        del grad
    return torch.stack(jacobians)


checkpoint = torch.load(checkpoint_path)
base_network = network_config["name"](**network_config["params"])
base_network.load_state_dict(checkpoint['base_network'])
base_network = base_network.cuda()
base_network.eval()

x_target = next(iter(loader_target))[0]


target_sensitivity = get_jacobian(x_target, class_num).mean()


jacobian_norms_rnd_per_x = []
jacobian_per_x = []
for x in tqdm(x_target):
    d1 = l2_normalize(torch.randn_like(x).to(x.device)) + x
    d2 = l2_normalize(torch.randn_like(x).to(x.device)) + x
    d3 = l2_normalize(torch.randn_like(x).to(x.device)) + x

    random_circle = get_fitted_ellipse_and_stats(90, torch.stack([d1, d2, d3]).cpu().numpy())
    random_circle = torch.from_numpy(random_circle).float()

    jacobian_norms_rnd = []
    for x_noisy in random_circle:
        sensitivity = get_jacobian(x_noisy[None], class_num).sum()
        jacobian_norms_rnd.append(sensitivity)
    
    jacobian_norms_rnd_per_x.append(torch.stack(jacobian_norms_rnd))
jacobian_norms_rnd_per_x = torch.stack(jacobian_norms_rnd_per_x)









loader_webcam = DataLoader(webcam_dataset, batch_size=3, shuffle=True, num_workers=4, drop_last=True)
loader_amazon = DataLoader(amazon_dataset, batch_size=3, shuffle=True, num_workers=4, drop_last=True)
loader_dslr = DataLoader(dslr_dataset, batch_size=3, shuffle=True, num_workers=4, drop_last=True)

if task[1] == "a":
	loader_target = loader_amazon
elif task[1] == "d":
	loader_target = loader_dslr
elif task[1] == "w":
	loader_target = loader_webcam


iter_target = iter(loader_target)
jacobian_norms_img_per_x = []

for l in tqdm(range(num_samples)):
    images = next(iter_target)[0]
    x_1 = images[0]
    x_2 = images[1]
    x_3 = images[2]

    random_circle = get_fitted_ellipse_and_stats(90, torch.stack([x_1, x_2, x_3]).cpu().numpy())
    random_circle = torch.from_numpy(random_circle).float()

    jacobian_norms_imgs = []
    for x_noisy in random_circle:
        sensitivity = get_jacobian(x_noisy[None], class_num).sum()
        jacobian_norms_imgs.append(sensitivity)

    jacobian_norms_img_per_x.append(torch.stack(jacobian_norms_imgs))

jacobian_norms_img_per_x = torch.stack(jacobian_norms_img_per_x)














shifted_rnd_per_x = jacobian_norms_rnd_per_x.mean(0).cpu()
shifted_rnd_per_x[:15] = jacobian_norms_rnd_per_x.mean(0).cpu()[75:]
shifted_rnd_per_x[15:] = jacobian_norms_rnd_per_x.mean(0).cpu()[:75]

shifted_img_per_x = jacobian_norms_img_per_x.mean(0).cpu()
shifted_img_per_x[:15] = jacobian_norms_img_per_x.mean(0).cpu()[75:]
shifted_img_per_x[15:] = jacobian_norms_img_per_x.mean(0).cpu()[:75]



plt.tight_layout()

plt.plot(shifted_img_per_x, marker="*", markersize=5, c="brown", label="an ellipse through 3 target images")
plt.plot(shifted_rnd_per_x, marker=".", markersize=5, c="b", label="a random ellipse")
plt.plot(np.arange(90), [target_sensitivity]*90, 'r--', label="target sensitivity")

max_val = float(int(shifted_img_per_x.max().item())) + 1.5
plt.ylim([0.0, max_val])
plt.xlim([-0.6, 90])

plt.legend(loc="upper right")
plt.legend(loc=(0.5, 0.9), fontsize=12, frameon=False)

plt.tick_params(axis='both', which='minor')
plt.xticks([15,45,75], ['$\pi$/3', '$\pi$', "3$\pi$/5"])
plt.xticks(fontsize=14)

xcoords = [15,45,75]
for xc in xcoords:
    plt.axvline(x=xc, linestyle='dotted', color=(0, 0, 0, 0.2))
    
plt.savefig(f'sensitivity/sensitivity_regions_{method}_{task}_{num_samples}.pdf',  bbox_inches='tight', pad_inches=0)

plt.clf()
plt.tight_layout()

plt.plot(shifted_img_per_x, marker="*", markersize=5, c="brown", label="an ellipse through 3 target images")
plt.plot(shifted_rnd_per_x, marker=".", markersize=5, c="b", label="a random ellipse")

max_val = float(int(shifted_img_per_x.max().item())) + 1.5
plt.ylim([0.0, max_val])
plt.xlim([-0.6, 90])

plt.legend(loc="upper right")
plt.legend(loc=(0.5, 0.9), fontsize=12, frameon=False)

plt.tick_params(axis='both', which='minor')
plt.xticks([15,45,75], ['$\pi$/3', '$\pi$', "3$\pi$/5"])
plt.xticks(fontsize=14)

xcoords = [15,45,75]
for xc in xcoords:
    plt.axvline(x=xc, linestyle='dotted', color=(0, 0, 0, 0.2))
    
plt.savefig(f'sensitivity/sensitivity_regions_{method}_{task}_{num_samples}_2.pdf',  bbox_inches='tight', pad_inches=0)


