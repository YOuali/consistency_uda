## Arguments ##
# iterations: 20k 
# seeds: 555 - 666 - 42
# augmentations: 4 
# consistency: yes
# using source labels in TDAN

###############################################################################
#######################            OFFICE31         ###########################
###############################################################################

#### WA

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run1_office31_wa --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run2_office31_wa --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run3_office31_wa --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run4_office31_wa --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run5_office31_wa --seed 100 --consistency --num_augs 4

#### DW

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run1_office31_dw --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run2_office31_dw --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run3_office31_dw --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run4_office31_dw --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run5_office31_dw --seed 100 --consistency --num_augs 4

#### DA

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run1_office31_da --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run2_office31_da --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run3_office31_da --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run4_office31_da --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/dslr_list.txt --method TDAN \
    --t_dset_path data/office/amazon_list.txt --output_dir run5_office31_da --seed 100 --consistency --num_augs 4

#### WD

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run1_office31_wd --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run2_office31_wd --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run3_office31_wd --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run4_office31_wd --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run5_office31_wd --seed 100 --consistency --num_augs 4


#### AD

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run1_office31_ad --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run2_office31_ad --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run3_office31_ad --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run4_office31_ad --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/dslr_list.txt --output_dir run5_office31_ad --seed 100 --consistency --num_augs 4

#### AW

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run1_office31_aw --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run2_office31_aw --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run3_office31_aw --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run4_office31_aw --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/amazon_list.txt --method TDAN \
    --t_dset_path data/office/webcam_list.txt --output_dir run5_office31_aw --seed 100 --consistency --num_augs 4

###############################################################################
#######################           image-clef        ###########################
###############################################################################

#### IC

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run1_imageclef_ic --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run2_imageclef_ic --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run3_imageclef_ic --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run4_imageclef_ic --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run5_imageclef_ic --seed 100 --consistency --num_augs 4

#### PI

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run1_imageclef_pi --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run2_imageclef_pi --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run3_imageclef_pi --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run4_imageclef_pi --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run5_imageclef_pi --seed 100 --consistency --num_augs 4

#### PC

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run1_imageclef_pc --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run2_imageclef_pc --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run3_imageclef_pc --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run4_imageclef_pc --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/p_list.txt --method TDAN \
    --t_dset_path data/image-clef/c_list.txt --output_dir run5_imageclef_pc --seed 100 --consistency --num_augs 4


#### IP

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run1_imageclef_ip --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run2_imageclef_ip --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run3_imageclef_ip --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run4_imageclef_ip --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/i_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run5_imageclef_ip --seed 100 --consistency --num_augs 4

#### CP

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run1_imageclef_cp --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run2_imageclef_cp --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run3_imageclef_cp --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run4_imageclef_cp --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/p_list.txt --output_dir run5_imageclef_cp --seed 100 --consistency --num_augs 4

#### CI

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run1_imageclef_ci --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run2_imageclef_ci --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run3_imageclef_ci --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run4_imageclef_ci --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset image-clef --test_interval 500 \
    --s_dset_path data/image-clef/c_list.txt --method TDAN \
    --t_dset_path data/image-clef/i_list.txt --output_dir run5_imageclef_ci --seed 100 --consistency --num_augs 4

###############################################################################
#######################         OFFICE HOME         ###########################
###############################################################################

#### Cl Ar

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run1_officehome_clar --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run2_officehome_clar --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run3_officehome_clar --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run4_officehome_clar --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run5_officehome_clar --seed 100 --consistency --num_augs 4

#### Pr Cl

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run1_officehome_prcl --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run2_officehome_prcl --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run3_officehome_prcl --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run4_officehome_prcl --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run5_officehome_prcl --seed 100 --consistency --num_augs 4

#### Pr Ar

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run1_officehome_prar --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run2_officehome_prar --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 --mean_teacher --vat \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run3_officehome_prar --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run4_officehome_prar --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run5_officehome_prar --seed 100 --consistency --num_augs 4

#### Cl Pr

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run1_officehome_clpr --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run2_officehome_clpr --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run3_officehome_clpr --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run4_officehome_clpr --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run5_officehome_clpr --seed 100 --consistency --num_augs 4

#### Ar Pr

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run1_officehome_arpr --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run2_officehome_arpr --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run3_officehome_arpr --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run4_officehome_arpr --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run5_officehome_arpr --seed 100 --consistency --num_augs 4

#### Rw Ar

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run1_officehome_rwar --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run2_officehome_rwar --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run3_officehome_rwar --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run4_officehome_rwar --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Art.txt --output_dir run5_officehome_rwar --seed 100 --consistency --num_augs 4

#### Pr Rw

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run1_officehome_prrw --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run2_officehome_prrw --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run3_officehome_prrw --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run4_officehome_prrw --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Product.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run5_officehome_prrw --seed 100 --consistency --num_augs 4

#### Rw Cl

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run1_officehome_rwcl --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run2_officehome_rwcl --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run3_officehome_rwcl --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run4_officehome_rwcl --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run5_officehome_rwcl --seed 100 --consistency --num_augs 4

#### Rw Pr

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run1_officehome_rwpr --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run2_officehome_rwpr --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run3_officehome_rwpr --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run4_officehome_rwpr --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Real_World.txt --method TDAN \
    --t_dset_path data/office-home/Product.txt --output_dir run5_officehome_rwpr --seed 100 --consistency --num_augs 4

#### Ar Cl

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run1_officehome_arcl --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run2_officehome_arcl --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run3_officehome_arcl --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run4_officehome_arcl --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Clipart.txt --output_dir run5_officehome_arcl --seed 100 --consistency --num_augs 4

#### Ar Rw

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run1_officehome_arrw --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run2_officehome_arrw --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run3_officehome_arrw --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run4_officehome_arrw --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Art.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run5_officehome_arrw --seed 100 --consistency --num_augs 4


#### Cl Rw

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run1_officehome_clrw --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run2_officehome_clrw --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run3_officehome_clrw --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run4_officehome_clrw --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset office-home --test_interval 500 \
    --s_dset_path data/office-home/Clipart.txt --method TDAN \
    --t_dset_path data/office-home/Real_World.txt --output_dir run5_officehome_clrw --seed 100 --consistency --num_augs 4

###############################################################################
#######################           REST              ###########################
###############################################################################

### VisDA 2017

python train_image.py --net ResNet50 --dset visda --test_interval 4000 --ramp_up 0.2 \
    --s_dset_path data/visda-2017/train_list.txt --method TDAN --num_iterations 40000 \
    --t_dset_path data/visda-2017/validation_list.txt --output_dir run1_visda --seed 42 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset visda --test_interval 4000 --ramp_up 0.2 \
    --s_dset_path data/visda-2017/train_list.txt --method TDAN --num_iterations 40000 \
    --t_dset_path data/visda-2017/validation_list.txt --output_dir run2_visda --seed 666 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset visda --test_interval 4000 --ramp_up 0.2 \
    --s_dset_path data/visda-2017/train_list.txt --method TDAN --num_iterations 40000 \
    --t_dset_path data/visda-2017/validation_list.txt --output_dir run3_visda --seed 555 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset visda --test_interval 4000 --ramp_up 0.2 \
    --s_dset_path data/visda-2017/train_list.txt --method TDAN --num_iterations 40000 \
    --t_dset_path data/visda-2017/validation_list.txt --output_dir run4_visda --seed 777 --consistency --num_augs 4

python train_image.py --net ResNet50 --dset visda --test_interval 4000 --ramp_up 0.2 \
    --s_dset_path data/visda-2017/train_list.txt --method TDAN --num_iterations 40000 \
    --t_dset_path data/visda-2017/validation_list.txt --output_dir run5_visda --seed 100 --consistency --num_augs 4

### SVHN->MNIST

python train_svhnmnist.py --epochs 50 --method TDAN \
    --seed 42 --consistency --num_augs 4

python train_svhnmnist.py --epochs 50 --method TDAN \
    --seed 666 --consistency --num_augs 4

python train_svhnmnist.py --epochs 50 --method TDAN \
    --seed 555 --consistency --num_augs 4

### USPS->MNIST

python train_uspsmnist.py --epochs 50 --task USPS2MNIST --method TDAN \
     --seed 42 --consistency --num_augs 4

python train_uspsmnist.py --epochs 50 --task USPS2MNIST --method TDAN \
     --seed 666 --consistency --num_augs 4

python train_uspsmnist.py --epochs 50 --task USPS2MNIST --method TDAN \
     --seed 555 --consistency --num_augs 4

### MNIST->USPS

python train_uspsmnist.py --epochs 50 --task MNIST2USPS --method TDAN \
     --seed 42 --consistency --num_augs 4

python train_uspsmnist.py --epochs 50 --task MNIST2USPS --method TDAN \
     --seed 666 --consistency --num_augs 4

python train_uspsmnist.py --epochs 50 --task MNIST2USPS --method TDAN \
     --seed 555 --consistency --num_augs 4
