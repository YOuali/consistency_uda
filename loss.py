import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import math
import torch.nn.functional as F
import pdb

CLS_IDX = None
PERM_IDX = None

def grl_hook(coeff):
    def fun1(grad):
        return -coeff*grad.clone()
    return fun1


def Entropy(input_):
    bs = input_.size(0)
    epsilon = 1e-5
    entropy = -input_ * torch.log(input_ + epsilon)
    entropy = torch.sum(entropy, dim=1)
    return entropy


def EntropyWeights(softmax_out, coeff):
    # Compute entropy loss
    entropy = Entropy(softmax_out)
    entropy.register_hook(grl_hook(coeff))
    entropy = 1.0+torch.exp(-entropy)
    # Source weights
    bs = softmax_out.size(0) // 2
    source_mask = torch.ones_like(entropy)
    source_mask[bs:] = 0
    source_weight = entropy*source_mask
    # Target weights
    target_mask = torch.ones_like(entropy)
    target_mask[0:bs] = 0
    target_weight = entropy*target_mask
    weight = source_weight / torch.sum(source_weight).detach().item() + \
        target_weight / torch.sum(target_weight).detach().item()
    return weight


def MSEWeights(softmax_out, softmax_out_gs, coeff):
    # Compute entropy loss
    mse_loss = F.mse_loss(softmax_out, softmax_out_gs, reduction='none')
    mse_loss = mse_loss.sum(1)
    mse_loss.register_hook(grl_hook(coeff))
    mse_weight = 1.0 + torch.exp(-mse_loss)
    # Source weights
    bs = softmax_out.size(0) // 2
    source_mask = torch.ones_like(mse_weight)
    source_mask[bs:] = 0
    source_weight = mse_weight*source_mask
    # Target weights
    target_mask = torch.ones_like(mse_weight)
    target_mask[0:bs] = 0
    target_weight = mse_weight*target_mask
    weight = source_weight / torch.sum(source_weight).detach().item() + \
        target_weight / torch.sum(target_weight).detach().item()
    return weight


"""
Transfer/ Invariance losses
"""


def CDAN(input_list, ad_net, entropy=False, coeff=None):
    softmax_output = input_list[1].detach()
    feature = input_list[0]
    # Tensorial product
    op_out = torch.bmm(softmax_output.unsqueeze(2), feature.unsqueeze(1))
    ad_out = ad_net(op_out.view(-1, softmax_output.size(1) * feature.size(1)))

    batch_size = softmax_output.size(0) // 2
    dc_target = torch.cat([torch.ones(batch_size, 1), torch.zeros(batch_size, 1)], dim=0)
    dc_target = dc_target.cuda()

    if entropy:
        weight = EntropyWeights(input_list[1], coeff)
        loss = nn.BCELoss(reduction='none')(ad_out, dc_target)
        return torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
    else:
        return nn.BCELoss()(ad_out, dc_target)


def DANN(input_list, ad_net, entropy=False, coeff=None):
    feature = input_list[0]
    ad_out = ad_net(feature)

    batch_size = ad_out.size(0) // 2
    dc_target = torch.cat([torch.ones(batch_size, 1), torch.zeros(batch_size, 1)], dim=0)
    dc_target = dc_target.cuda()

    if entropy:
        weight = EntropyWeights(input_list[1], coeff)
        loss = nn.BCELoss(reduction='none')(ad_out, dc_target)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    else:
        loss = nn.BCELoss()(ad_out, dc_target)
        return loss


def TDAN(input_list, ad_net, entropy=False, mse=False, coeff=None, softmax_out=None, softmax_out_gs=None):
    softmax_output = input_list[1].detach()
    feature = input_list[0]
    ad_out = ad_net(feature)

    batch_size, num_cls = ad_out.size(0) // 2, ad_out.size(1)
    dc_target = torch.cat([torch.ones(batch_size, num_cls), torch.zeros(batch_size, num_cls)], dim=0)
    dc_target = dc_target.cuda()

    if mse:
        weight = MSEWeights(softmax_out, softmax_out_gs, coeff)
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).sum(dim=1, keepdim=True)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    elif entropy:
        weight = EntropyWeights(softmax_out, coeff)
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).sum(dim=1, keepdim=True)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    else:
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=0).sum()
        return loss


def TDAN_tensorial_product(input_list, ad_net, entropy=False, coeff=None):
    softmax_output = input_list[1].detach()
    feature = input_list[0]
    # Tensorial product
    op_out = torch.bmm(softmax_output.unsqueeze(2), feature.unsqueeze(1))
    ad_out = ad_net(op_out.view(-1, softmax_output.size(1) * feature.size(1)))

    batch_size, num_cls = ad_out.size(0) // 2, ad_out.size(1)
    dc_target = torch.cat([torch.ones(batch_size, num_cls), torch.zeros(batch_size, num_cls)], dim=0)
    dc_target = dc_target.cuda()

    if entropy:
        weight = EntropyWeights(input_list[1], coeff)
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=1, keepdim=True)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    else:
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=0).sum()
        return loss


"""
Consistency losses
"""

def softmax_entropy_loss(inputs, targets):
    assert inputs.size() == targets.size()
    inputs = F.softmax(inputs, dim=1)
    targets = F.softmax(targets, dim=1)
    loss = 1 - (inputs * targets).mean(0).sum()
    return loss

def softmax_mse_loss(inputs, targets):
    assert inputs.size() == targets.size()
    inputs = F.softmax(inputs, dim=1)
    targets = F.softmax(targets, dim=1)
    loss = F.mse_loss(inputs, targets, reduction='mean')
    return loss

def softmax_kl_loss(inputs, targets):
    assert inputs.size() == targets.size()
    input_log_softmax = F.log_softmax(inputs, dim=1)
    targets = F.softmax(targets, dim=1)
    loss = F.kl_div(input_log_softmax, targets, reduction='mean')
    return loss

def softmax_js_loss(inputs, targets):
    assert inputs.size() == targets.size()
    inputs = F.softmax(inputs, dim=1)
    targets = F.softmax(targets, dim=1)
    # Clamp mixture distribution to avoid exploding KL divergence
    p_mixture = torch.clamp((inputs + targets) / 2., 1e-7, 1).log()
    loss = F.kl_div(p_mixture, inputs, reduction='mean') + \
                F.kl_div(p_mixture, targets, reduction='mean')
    return loss


"""
Confident loss
"""


def confidence_loss(pred):
    """
    KL divergence between unifrom dist and the predictions
    https://arxiv.org/pdf/1908.09822.pdf
    """
    num_class = pred.shape[1]
    logsoftmax = F.log_softmax(pred, dim=1)
    loss = torch.mean(-logsoftmax/num_class)
    #loss = torch.sum(-logsoftmax/num_class)
    return loss



"""
TDAN losses for stability tests
"""

def TDAN_argmax(input_list, ad_net, entropy=False, coeff=None):
    softmax_output = input_list[1].detach()
    feature = input_list[0]
    ad_out = ad_net(feature)

    batch_size, num_cls = ad_out.size(0) // 2, ad_out.size(1)
    dc_target = torch.cat([torch.ones(batch_size, num_cls), torch.zeros(batch_size, num_cls)], dim=0)
    dc_target = dc_target.cuda()

    # One hot vectors
    softmax_output_onehot = torch.zeros_like(softmax_output)
    softmax_output_onehot[torch.arange(ad_out.size(0)), softmax_output.argmax(1)] = 1.0

    if entropy:
        weight = EntropyWeights(input_list[1], coeff)
        loss = (softmax_output_onehot * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=1, keepdim=True)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    else:
        loss = (softmax_output_onehot * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=0).sum()
        return loss


def TDAN_random(input_list, ad_net, entropy=False, coeff=None, random_percentage=0.1, first=True):
    softmax_output = input_list[1].detach()

    feature = input_list[0]
    ad_out = ad_net(feature)

    batch_size, num_cls = ad_out.size(0) // 2, ad_out.size(1)
    dc_target = torch.cat([torch.ones(batch_size, num_cls), torch.zeros(batch_size, num_cls)], dim=0)
    dc_target = dc_target.cuda()

    if first:
        # Randomly permute 10% of classes
        num_cls_to_permute = int(num_cls * random_percentage) + 1
        cls_start = np.random.randint(0, num_cls - num_cls_to_permute)
        CLS_IDX = np.arange(cls_start, cls_start+num_cls_to_permute)
        PERM_IDX = np.random.permutation(CLS_IDX)
        first = False

    softmax_output[batch_size:, CLS_IDX] = softmax_output[batch_size:, PERM_IDX]

    if entropy:
        weight = EntropyWeights(input_list[1], coeff)
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=1, keepdim=True)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    else:
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=0).sum()
        return loss


"""
For multi source DA: digits
"""

def TDAN_msda_digit(input_list, ad_net, entropy=False, coeff=None):
    softmax_output = input_list[1].detach()
    feature = input_list[0]
    ad_out = ad_net(feature)

    batch_size, num_cls = ad_out.size(0) // 5, ad_out.size(1)
    dc_target = torch.cat([torch.ones(batch_size*4, num_cls), torch.zeros(batch_size, num_cls)], dim=0)
    dc_target = dc_target.cuda()

    if entropy:
        weight = EntropyWeights(input_list[1], coeff)
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=1, keepdim=True)
        loss = torch.sum(weight.view(-1, 1) * loss) / torch.sum(weight).detach().item()
        return loss
    else:
        loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=0).sum()
        return loss


