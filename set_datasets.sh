#!/bin/bash
DATA_PATH=$1
CUR_PATH=`/bin/pwd`

# Create folders for datasets
cd $DATA_PATH
DATA_PATH=`/bin/pwd`
mkdir -p DA_datasets
cd DA_datasets
mkdir -p svhn2mnist
mkdir -p usps2mnist
mkdir -p office-home
mkdir -p office
mkdir -p image-clef
mkdir -p visda-2017

# Google download
function gdrive_download () {
  CONFIRM=$(wget --quiet --save-cookies cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$1" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
  wget --load-cookies cookies.txt "https://docs.google.com/uc?export=download&confirm=$CONFIRM&id=$1" -O $2
  rm -f cookies.txt
}

# svhn2mnist
cd svhn2mnist

SVHN_FILEID="1Y0wT_ElbDcnFxtu25MB74npURwwijEdT"
gdrive_download $SVHN_FILEID svhn_image.tar.gz
tar xf svhn_image.tar.gz

wget https://github.com/thuml/CDAN/raw/master/data/svhn2mnist/mnist_image.tar.gz
tar xf mnist_image.tar.gz

# usps2mnist
cd ../usps2mnist

wget https://github.com/thuml/CDAN/raw/master/data/usps2mnist/images.tar.gz
tar xf images.tar.gz

# office-home
cd ../office-home

HOME_FILEID="0B81rNlvomiwed0V1YUxQdC1uOTg"
gdrive_download $HOME_FILEID OfficeHomeDataset_10072016.zip

unzip -q OfficeHomeDataset_10072016.zip
mv OfficeHomeDataset_10072016 images

# office
cd ../office

OFFH_FILEID="0B4IapRTv9pJ1WGZVd1VDMmhwdlE"
gdrive_download $OFFH_FILEID domain_adaptation_images.tar.gz

mkdir domain_adaptation_images
tar xf domain_adaptation_images.tar.gz -C domain_adaptation_images

# image-clef
cd ../image-clef

CLEF_FILEID="0B9kJH0-rJ2uRS3JILThaQXJhQlk"
gdrive_download $CLEF_FILEID image-clef.tar.gz

tar xf image-clef.tar.gz
mv image-clef/* .
rm -rf image-clef

# Visda-2017
cd ../visda-2017

wget http://csr.bu.edu/ftp/visda17/clf/train.tar
tar xf train.tar

wget http://csr.bu.edu/ftp/visda17/clf/validation.tar
tar xf validation.tar  

wget http://csr.bu.edu/ftp/visda17/clf/test.tar
tar xf test.tar

wget https://raw.githubusercontent.com/VisionLearningGroup/taskcv-2017-public/master/classification/data/image_list.txt


# Digit-Five 
cd ..

DIGIT_FILEID="1A4RJOFj4BJkmliiEL7g9WzNIDUHLxfmm"
gdrive_download $DIGIT_FILEID Digit-Five.zip
unzip Digit-Five.zip

# Linking
ln -s $DATA_PATH"/DA_datasets" $CUR_PATH"/data"