# Consistency Regularization & UDA

## To Do

- [x] Fix dataset paths and create a simple bash script to download and set a link to the datasets. 
- [x] Create augmentations (similar to AutoAugment/RandAugment) and dataset wrapper for strong and weak augmentations.
- [x] Implement consistency loss & the ramp-up functions.
- [x] Implement all possible training cases (only source, dann, cdan and each one with consistency)
- [x] Add the necessary flags for parameter selection.
- [x] Add average meter for loss printing (set to zero when S/T are reset).
- [x] Better printing.
- [x] Better model saving.
- [x] Model reloading (backbone & ad net).
- [x] Implement KL loss to avoid overconfident prediction on target.
- [x] Implementing code for automatically computing beta (takes a pretrained backbones, done in `train_image` directly).
- [x] First tests.
- [x] Add an option for an exp weighted avg for generating target predictions.
- [x] Implementing argmax and random versions of TDAN.
- [x] The mesures in Transferability vs. Discriminability paper: train MLP on target too, SVD, LDA, corresponding angle (*not tested*).
- [x] Replicating the changes of `train_image` to `train_svhn` and `train_usps` (note: change IMAGE_SIZE in augmentations).
- [x] Added tensorial product version of TDAN.
- [ ] After reloading, start from last iter / change the lr coeficients (not urgent).
- [ ] Maybe loading the model for ema? (not urgent).
- [x] Added multi-source domain adaptation for digits as in [MSDA](https://github.com/VisionLearningGroup/VisionLearningGroup.github.io/tree/master/M3SDA/code_MSDA_digit).

## Code changes

- Removed AlexNet from `network.py`.
- Removed AlexNet normalization from `pre_process.py`.
- Fixing seeds on training for stable and fair comparaison.
- Renamed `data_list` to `datasets`.
- Removed ImageValueList from datasets.
- Removed GPU id setup.
- Removed Random projection, CDAN is now only tensorial product.
- Entropy weighting is now a seperate parameter.
- For training on source only, the method is called `BASELINE`, transfer loss is `TDAN`.
- Renamed `lr_schedule` to `mics` for more general usage.
- Simplified transfer loss computation.
- To reduce memory, we only save two versions of the model: last and best.

## Observations:
- It seems that consistency loss's value needs to be similar to the classification loss, if not, the accuracy drops.

# Ablation: Number of Augmentations

| Method   | N = 0  | N = 1  | N = 2  | N = 3 | N = 4  | N = 5  | N = 6 |
| -------- |:------:|:------:|------:|:------:|:------:|-------:|------:|
| D -> A   |  74.5  | 75.2   | 75.6  |  75.9  |  76.0  |  75.5  | 75.9  |
| D -> A   |  73.4  | 76.3   | 76.1  |  75.9  |  76.4  |  76.3  | 76.4  |
| Mean     |  73.9  | 75.7   | 75.8  |  75.9  |  76.2  |  75.9  | 76.2    |

| Method   | N = 0  | N = 1  | N = 2  | N = 3 | N = 4  | N = 5  | N = 6 |
| -------- |:------:|:------:|------:|:------:|:------:|-------:|------:|
| W -> A   |  71.5  | 72.7   | 72.1  |  73.1  |  72.7  |  74.3  | 72.3  |
| W -> A   |  71.9  | 71.9   | 72.7  |  72.4  |  73.0  |  72.4  | 72.1  |
| Mean     |  71.7  | 72.3   | 72.4  |  72.7  |  72.8  |  73.3  | 72.3  |

# Ablation: Office31 - Ramp up 3500 - 15000 iterations

TDAN & Consistency 

| AW   | DW   | WD   | AD  | DA  |  WA  | AVG  |
|:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| 95.3 | 99.2 | 100.0| 93.7| 75.6| 73.1 | 89.4 |
| 96.2 | 99.2 | 100.0| 92.3| 74.4| 73.4 | 89.2 |
| 93.7 | 99.2 | 100.0| 92.5| 76.7| 73.5 | 89.2 |

TDAN & Consistency & VAT - 5K

| AW   | DW   | WD   | AD  | DA  |  WA  | AVG  |
|:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| 93.9 | 99.2 | 100.0| 92.3| 76.6| 73.5 | 89.2 |
| 94.3 | 98.9 | 100.0| 93.3| 75.3| 72.8 | 89.1 |
| 94.9 | 99.2 | 100.0| 92.1| 74.5| 73.2 | 88.9 |

TDAN & Consistency

| AW   | DW   | WD   | AD  | DA  |  WA  | AVG  |
|:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| 93.7 | 99.2 | 100.0| 93.1| 75.6| 73.1 | 89.1 |
| 93.7 | 98.9 | 100.0| 94.5| 75.1| 72.7 | 89.1 |
| 95.3 | 99.1 | 100.0| 93.9| 75.7| 73.1 | 89.5 |

TDAN & VAT

| AW   | DW   | WD   | AD  | DA  |  WA  | AVG  |
|:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| 94.0 | 98.9 | 100.0| 94.7| 75.2| 72.9 | 89.2 |
| 93.4 | 98.9 | 100.0| 94.9| 73.6| 72.3 | 88.8 |
| 93.7 | 98.7 | 100.0| 94.5| 72.8| 73.7 | 88.9 |

TDAN

| AW   | DW   | WD   | AD  | DA  |  WA  | AVG  |
|:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| 93.2 | 98.6 | 100.0| 92.5| 74.0| 72.1 | 88.4 |
| 94.9 | 98.9 | 100.0| 93.7| 72.9|   .  |      |
| 95.5 | 98.7 | 100.0| 93.9| 69.8|   .  |      |

## Robustness
Note: here no consistency is applied

| Method            | D -> A | W -> A | visda |
| ------------------|:------:|:------:|------:|
| TDAN              | 74.0   | 71.2   | 68.7  |
| TDAN onehot       | 74.2   | 71.9   | 68.6  |
| TDAN vat onehot   | 71.1   | 68.1   | 68.1  |
| TDAN+smooth0.1    | 74.6   | 71.8   | 68.7  |
| TDAN+smooth0.3    | 72.7   | 70.9   | 70.3  |
| TDAN+smooth0.5    | 73.1   | 70.6   | 68.3  |

TDAN = TDAN + consistency

| Method            | D -> A | W -> A | visda |
| ------------------|:------:|:------:|------:|
| TDAN no const     | 74.0 / 73.8 | 71.2 / 71.8  | 68.7 / 67.1 |
| TDAN              | 76.2 / 75.9 | 73.3 / 72.8  | 75.3 / 73.0 |
| TDAN+DataAug      | 73.8 / 74.3 | 70.8 / 71.6  | 69.3 / 67.3 |
| TDAN+Sensitivity  | 73.0 / 72.3 | 70.9 / 70.9  | 69.0 / 69.0 |
|TDAN+SensitivityVAT|  71.2/71.4  | 69.5 / 70.8  | 69.1 / 66.8 |
| TDAN+VAT          | 74.9 / 75.0 | 72.0 / 72.5  | 74.1 / 73.1 |
| TDAN+VAT+Trsf     |  76.1/76.1  | 74.0 / 73.0  | 77.1 / 76.6 |
| New Loss (mean)   | 74.7 / 72.9 | 71.0 / 71.5  | 71.1 / 70.6 |
|New Loss (sum_mean, lambda=1)| 75.2 / 75.5 |  72.6 / 72.2 | 75.2 / 71.0 |

| Method            | D -> A | W -> A | visda |
| ------------------|:------:|:------:|------:|
|MSE weight - both detached| 69.1  |  73.2  | 72.5 |
|MSE weight - gs detached| 66.8 | 66.0  | 71.1 |
|Entropy | 75.5 | 72.0  |  75.1 |

| Method | D -> A   | W -> A   |
| ------ |:----:|:----:|
| TDAN no const   | 74.5 | 71.7 |
| TDAN   | 75.7 | 73.0 |
| TDAN+Random   | 75.3 | 71.7 |
| TDAN+detach   | 75.5 | 70.9 |

## No sonsistency
| Method | D -> A   | W -> A   |
| ------ |:----:|:----:|
| Just consistency   | 66.9 | 65.4 |

## Runs

- iterations: 20k 
- seeds: 555 - 666 - 42 - 777 - 100
- augmentations: 4 
- consistency: yes
- using source labels in TDAN

### Office31

| Method | AW   | DW   | WD   | AD  | DA  |  WA  | AVG  |
| ------ |:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| STOA   | 93.1 | 99.3 |100.0 | 93.2| 73.1| 72.1 | 88.4 |
| TDAN   | 95.3 | 99.2 |100.0 | 93.9| 75.0| 74.6 | 89.6 |
| TDAN   | 95.5 | 99.1 |100.0 | 94.9| 74.3| 73.3 | 89.5 |
| TDAN   | 94.3 | 99.2 |100.0 | 95.1| 76.2| 73.5 | 89.7 |
| TDAN   | 94.7 | 98.7 |100.0 | 95.9| 75.5| 73.3 | 89.6 |
| TDAN   | 96.1 | 99.2 |100.0 | 94.1| 74.0| 74.0 | 89.5 |

### Image Clef

| Method | IP   | PI   | IC   | CI  | CP  | PC   | AVG  |
| ------ |:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| STOA   | 78.8 | 92.0 | 97.5 | 92.0| 78.2| 94.7 | 88.9 |
| TDAN   | 80.6 | 92.5 | 97.6 | 93.0| 77.6| 95.3 | 89.4 |
| TDAN   | 79.5 | 93.1 | 97.1 | 93.8| 79.0| 94.8 | 89.5 |
| TDAN   | 79.5 | 91.6 | 97.8 | 92.3| 78.0| 94.6 | 88.9 |
| TDAN   | 78.6 | 93.6 | 97.3 | 93.0| 78.1| 96.5 | 89.5 |
| TDAN   | 79.3 | 92.6 | 97.3 | 93.0| 78.1| 95.6 | 89.3 |

### Office Home

| Method | AR-CL | AR-PR | AR-RW | CL-AR | CL-PR | CL-RW | PR-AR | PR-CL | PR-RW | RW-AR | RW-CL| RW-PR|  AVG  |
| ------ |:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:----:|:----:|:-----:|
| STOA   |  51.6 |  69.5 |  75.4 |  59.4 | 69.5  |  68.6 |  59.5 |  50.5 |  76.8 | 70.9  | 56.6 | 81.6 |  65.8 |
| TDAN   |  52.8 |  72.7 |  77.1 |  62.1 | 71.7  |  72.8 |  62.1 |  51.8 |  79.9 | 73.3  | 59.2 | 83.4 |  68.2 |
| TDAN   |  52.4 |  73.6 |  77.6 |  62.0 | 72.4  |  73.5 |  62.1 |  54.3 |  79.9 | 75.1  | 59.1 | 83.2 |  68.7 |
| TDAN   |  51.7 |  69.9 |  76.9 |  62.9 | 70.7  |  72.8 |  63.7 |  53.5 |  79.9 | 73.7  | 60.5 | 83.6 |  68.3 |
| TDAN   |  51.3 |  71.0 |  76.2 |  62.4 | 72.8  |  73.4 |  63.1 |  53.4 |  80.0 | 74.7  | 59.5 | 82.6 |  68.3 |
| TDAN   |  51.9 |  73.4 |  77.3 |  62.9 | 71.9  |  73.0 |  62.1 |  53.6 |  79.7 | 74.2  | 59.8 | 83.4 |  68.6 |


### EMA

| Method | D -> A   | W -> A   |
| ------ |:----:|:----:|
| TDAN no const   | 74.5 | 71.7 |
| TDAN   | 75.7 | 73.0 |
| TDAN+EMA   | 74.3 | 72.8 |
| TDAN+EMA T=3   | 71.4 | 71.6 |
| TDAN+EMA v2   | 76.2 | 72.2 | 


### Rest

| Method | VisDa   | MNIST2USPS   | USPS2MNIST   |  SVHN2MNIST | AVG   |
| ------ |:-------:|:------------:|:------------:|:-----------:|:-----:|
| STOA   |   71.9  |      96.0    |     98.0     |    90.5     | 94.3  |
| TDAN   |   75.5  |              |              |             |       |
| TDAN   |   76.2  |              |              |             |       |
| TDAN   |   72.1  |              |              |             |       |
| TDAN   |   75.8  |              |              |             |       |
| TDAN   |   75.8  |              |              |             |       |


## Test Results

Results of `W -> A` on Office31 for 30k iterations (fixed seed, one run).

#### Best consistency loss weighting

| Method                | Target Precision | Lambda consistency |
| --------------------- |:----------------:|:------------------:|
| ResNet                | 60.95            | -                  |
| ResNet+Consistency    | 64.32            | 5  (Very stable)   |
| ResNet+Consistency    | 64.14            | 10 (Very stable)   |
| ResNet+Consistency    | 64.28            | 15 (Kinda stable)  |
| ResNet+Consistency    | 63.08            | 30 (Not stable)    |
| ResNet+Consistency    | 63.36            | 50 (Not Stable)    |

#### With and without consistency

Run 1 (seed 42)

| Method                | Target Precision  |
| --------------------- |:-----------------:|
| ResNet                | 60.95             |
| DANN                  | 70.57             |
| CDAN                  | 71.03             |
| TDAN                  | 71.31             |
| ResNet+Consistency    | 64.14             |
| DANN+Consistency      | 71.45             |
| CDAN+Consistency      | 74.65             |
| TDAN+Consistency      | 73.16             |

Run 2 (seed 0 or 1)

| Method                | Target Precision  |
| --------------------- |:-----------------:|
| DANN                  | 70.25             |
| CDAN                  | 69.64             |
| TDAN                  | 71.56             |
| TDAN+Tens Pord        | 69.32             |
| DANN+Consistency      | 71.81             |
| CDAN+Consistency      | 74.65             |
| TDAN+Consistency      | 73.69             |
| TDAN+Tens Pord+Const  | 71.45             |

#### Office31

First test runs
Lambda = 5, iterations = 20k, seeds = 666 & 777.


| Method | AW   | DW   | WD   | AD  | DA  | WA   | AVG  |
| ------ |:----:|:----:|:----:|:---:|:---:|:----:|:----:|
| STOA   | 93.1 | 99.3 | 100.0| 93.2| 73.1| 72.1 | 88.4 |
| CDAN   | 93.4 | 99.1 | 100.0| 93.2| 72.3| 69.0 | 87.8 |
| TDAN   | 93.9 | 99.1 | 100.0| 94.5| 73.4| 72.9 | 88.9 |
|    |  |  | | | | | |
| CDAN   | 93.8 | 98.8 | 99.7 | 92.9| 74.8| 68.9 | 88.1 |
| TDAN   | 94.9 | 98.3 | 100.0| 94.5| 74.3| 73.4 | 89.2 |


#### Why is CDAN this low ?

| CDAN                        | Acc    |
| --------------------------- |:------:|
| seed 666, lambda = 5, 20k   | 69.00  |
| seed 666, lambda = 10, 20k  | 69.43  |
| seed 666, lambda = 10, 20k  | 68.19  |
| seed 666, lambda = 5, 30k   | 69.00  |
| seed 777, lambda = 5, 20k   | 68.97  |
| seed 0, lambda = 10, 20k    | 71.92  |
| seed 42, lambda = 10, 20k   | 75.64  |

#### Strong and Weak augmentations

Lambda = 10, iterations = 20k, seed = 666, Office31 W -> A.

| TDAN                | Acc     |
| ------------------- |:-------:|
| 1                   | 72.41  |
| 2                   | 72.34  |
| 3                   | 73.51  |
| 4                   | 73.58  |

#### Beta

| Method                | With Cons  | W/t Cons | Beta |
| --------------------- |:----------:|:--------:|:----:|
| CDAN                  | 74.01      | 69.82    | 0.94 |
| TDAN                  | 72.59      | 71.03    | 0.97 |

#### Stability of `TDAN`

| Method            | Target Precision  |
| ----------------- |:-----------------:|
| TDAN              |                   |
| TDAN+10% random   |                   |
| TDAN+argmax       |                   |

#### Exponentially moving average

| Method                | Target Precision (no ema)|
| --------------------- |:-----------------:|
| CDAN+Cons+EMA         | 71.35(72.45)      |
| TDAN+Cons+EMA         | 73.34(72.09)      |

#### Weighting

| Method              | Target Precision | Lambda consistency |
| --------------------|:----------------:|:------------------:|
| CDAN                | 69.29            | 10                 |
| CDAN                | 68.79            | 20                 |
| TDAN                | 67.27            | 10                 |
| TDAN                | 68.40            | 20                 |

#### True source labels

| Method              | With    | Without |
| --------------------|:-------:|:-------:|
| TDAN                | 71.81   | 71.49   |
| TDAN+Const          | 72.09   | 72.80   |

## Datasets:

Simply run:

```
sh set_datasets.sh PATH_TO_STORE
```

This will automatically download all of the domain adaptation datasets to be stored in `PATH_TO_STORE` and link the downloaded dataset to `data/`

## Training

```
SVHN->MNIST
python train_svhnmnist.py --epochs 50

USPS->MNIST
python train_uspsmnist.py --epochs 50 --task USPS2MNIST

MNIST->USPS
python train_uspsmnist.py --gpu_id id--epochs 50 --task MNIST2USPS
```


```
Office-31

python train_image.py --net ResNet50 --dset office --test_interval 500 \
    --s_dset_path data/office/webcam_list.txt \
    --t_dset_path data/office/amazon_list.txt
```


```
Office-Home

python train_image.py --net ResNet50 --dset office-home \
    --test_interval 2000 --s_dset_path data/office-home/Art.txt \
    --t_dset_path data/office-home/Clipart.txt --output_dir test
```
```
VisDA 2017

python train_image.py --net ResNet50 --dset visda \
    --test_interval 5000 --s_dset_path data/visda-2017/train_list.txt \
    --t_dset_path data/visda-2017/validation_list.txt --output_dir test
```


```
Image-clef

python train_image.py --net ResNet50 --dset image-clef \
    --test_interval 500 --s_dset_path data/image-clef/b_list.txt \
    --t_dset_path data/image-clef/i_list.txt --output_dir test
```


## Possible flags & Training options

```
usage: train_image.py [-h] [--seed SEED] [--method {BASELINE,TDAN,CDAN,DANN}]
                      [--net {ResNet18,ResNet34,ResNet50,ResNet101,ResNet152}]
                      [--entropy] [--confidence]
                      [--num_iterations NUM_ITERATIONS]
                      [--test_interval TEST_INTERVAL] [--resume RESUME]
                      [--train_classifier]
                      [--dset {office,image-clef,visda,office-home}]
                      [--s_dset_path S_DSET_PATH] [--t_dset_path T_DSET_PATH]
                      [--snapshot_interval SNAPSHOT_INTERVAL]
                      [--output_dir OUTPUT_DIR] [--lr LR]
                      [--lambda_transfer LAMBDA_TRANSFER]
                      [--lambda_consistency LAMBDA_CONSISTENCY]
                      [--lambda_confidence LAMBDA_CONFIDENCE]
                      [--aug_severity AUG_SEVERITY] [--num_augs NUM_AUGS]
                      [--consistency] [--possible_augs POSSIBLE_AUGS]
                      [--ramp_up RAMP_UP] [--mean_teacher]

Consistency based domain adaptation

optional arguments:
  -h, --help            show this help message and exit
  --seed SEED
  --method {BASELINE,TDAN,CDAN,DANN}
  --net {ResNet18,ResNet34,ResNet50,ResNet101,ResNet152}
  --entropy             Add entropy weighting (as in CDAN)
  --confidence          Add confidence loss to avoid confident predictions on
                        target
  --num_iterations NUM_ITERATIONS
  --test_interval TEST_INTERVAL
                        interval of two continuous test phase
  --resume RESUME       Path to .pth model
  --train_classifier    Train classfier on top of a frozen feature extractor
  --dset {office,image-clef,visda,office-home}
                        The dataset or source dataset used
  --s_dset_path S_DSET_PATH
                        The source dataset path list
  --t_dset_path T_DSET_PATH
                        The target dataset path list
  --snapshot_interval SNAPSHOT_INTERVAL
                        interval of two continuous output model
  --output_dir OUTPUT_DIR
                        output directory of our model (in ../snapshot
                        directory)
  --lr LR               learning rate
  --lambda_transfer LAMBDA_TRANSFER
                        Loss trade-off for transfer term
  --lambda_consistency LAMBDA_CONSISTENCY
                        Loss trade-off for consistency term
  --lambda_confidence LAMBDA_CONFIDENCE
                        Loss trade-off for confidence term
  --aug_severity AUG_SEVERITY
                        Severity of base augmentation operators
  --num_augs NUM_AUGS   Number of augmentations to apply
  --consistency         Use consistency loss
  --possible_augs POSSIBLE_AUGS
                        Size of the set of augmentations to sample from (-1:
                        all of 13 augs)
  --ramp_up RAMP_UP     Consistency loss rampup periode (ramps from 0. to 1.
                        in the first 35 percent of iters)
  --mean_teacher        Use a teacher model to generated the target
                        predictions
```
